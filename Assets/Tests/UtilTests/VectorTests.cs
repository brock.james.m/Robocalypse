using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

using Robo.Util.Vector;


public class VectorTests
{
    [Test]
    public void VectorRoughlyEqualsEnforcesThreeDecimalPrecision()
    {
        Vector2 oneDecimalPrecision = new Vector2(0.1f, 0.0f);
        Vector2 twoDecimalPrecision = new Vector2(0.02f, 0.0f);
        Vector2 threeDecimalPrecision = new Vector2(0.0f, 0.001f);
        Vector2 threeDecimalPrecisionTooFar = new Vector2(0.0f, 0.003f);
        Vector2 fourDecimalPrecision = new Vector2(0.0f, 0.0004f);

        Assert.IsFalse(oneDecimalPrecision.RoughlyEquals(Vector2.zero));
        Assert.IsFalse(twoDecimalPrecision.RoughlyEquals(Vector2.zero));
        Assert.IsFalse(threeDecimalPrecisionTooFar.RoughlyEquals(Vector2.zero));
        Assert.IsTrue(threeDecimalPrecision.RoughlyEquals(Vector2.zero));
        Assert.IsTrue(fourDecimalPrecision.RoughlyEquals(Vector2.zero));
    }

    [Test]
    public void VectorRotateKeepsMagnitudeAndIsRotated()
    {
        // Rotations are in degrees counter-clockwise from Vector2.up
        Vector2 v = Vector2.up;
        Assert.IsTrue(v.Rotate(0.0f).RoughlyEquals(Vector2.up));
        Assert.IsTrue(v.Rotate(90.0f).RoughlyEquals(Vector2.left));
        Assert.IsTrue(v.Rotate(180.0f).RoughlyEquals(Vector2.down));
        Assert.IsTrue(v.Rotate(-180.0f).RoughlyEquals(Vector2.down));
        Assert.IsTrue(v.Rotate(-90.0f).RoughlyEquals(Vector2.right));


        v = new Vector2(-2.0f, 2.0f);
        Assert.IsTrue(v.Rotate(0.0f).RoughlyEquals(v));
        Assert.IsTrue(v.Rotate(90.0f).RoughlyEquals(new Vector2(-2.0f, -2.0f)));
        Assert.IsTrue(v.Rotate(-90.0f).RoughlyEquals(new Vector2(2.0f, 2.0f)));
    }
}
