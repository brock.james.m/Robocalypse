using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;


using Robo.Util;


public class TimerTests
{
    private float TestTimerDuration = 0.1f;

    [UnityTest]
    public IEnumerator TimerTestsSimplePasses()
    {
        Timer testTimer = new Timer(duration: TestTimerDuration, startTime: Time.time);

        float prevElapsedTime = testTimer.ElapsedTime;
        float prevElapsedPercent = testTimer.ElapsedTime;

        Assert.IsTrue(testTimer.IsActive);

        while (testTimer.IsActive)
        {
            // Process Unity Frame
            yield return null;

            Assert.GreaterOrEqual(testTimer.ElapsedTime, prevElapsedTime);
            Assert.GreaterOrEqual(testTimer.ElapsedTime, prevElapsedPercent);

            prevElapsedTime = testTimer.ElapsedTime;
            prevElapsedPercent = testTimer.ElapsedTime;
        }

        Assert.IsFalse(testTimer.IsActive);
        Assert.AreEqual(testTimer.ElapsedTime, TestTimerDuration);
        Assert.AreEqual(testTimer.ElapsedPercent, 1.0f);
    }
}
