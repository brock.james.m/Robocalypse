using NUnit.Framework;
using Moq;

using Robo.Event;
using Robo.Event.Interface;


public class DispatcherTests
{
    protected Mock<IEffectProcessor> mockHealthProcessor;
    protected Mock<IEffectProcessor> mockMoverProcessor;
    protected IEffectDispatcher effectDispatcher;
    protected IEventDispatcher eventDispatcher;

    protected const int meleeAttackDamage = 1;
    protected const float meleeKnockbackDuration = 0.4f;
    protected const float meleeKnockbackSpeed = 0.2f;
    protected IEffect meleeDamageEffect;
    protected IEffect meleeKnockbackEffect;
    protected IEvent meleeAttackWithDamage;
    protected IEvent meleeAttackWithDamageAndKnockback;


    [SetUp]
    public void Setup()
    {
        Robo.Log.Logger.Level = Robo.Log.Logger.LogLevel.Debug;

        meleeDamageEffect = new DamageEffect(meleeAttackDamage);
        meleeKnockbackEffect =
            new MoveEffect(
                duration: meleeKnockbackDuration,
                speed: MoveEffect.KnockbackSpeed(1.0f));

        meleeAttackWithDamage =
                new TriggerEventBuilder().Builder()
                    .WithEffect(meleeDamageEffect)
                .Build();

        meleeAttackWithDamageAndKnockback =
                new TriggerEventBuilder().Builder()
                    .WithEffect(meleeDamageEffect)
                    .WithEffect(meleeKnockbackEffect)
                .Build();

        mockHealthProcessor = new Mock<IEffectProcessor>();
        mockHealthProcessor
            .Setup(p => p.ProcessEffect(It.IsAny<BaseEffect>()))
            .Returns(DispatchStatus.Success);

        mockMoverProcessor = new Mock<IEffectProcessor>();
        mockMoverProcessor
            .Setup(p => p.ProcessEffect(It.IsAny<BaseEffect>()))
            .Returns(DispatchStatus.Success);

        effectDispatcher
                = new EffectDispatcher(
                    healthProcessor: mockHealthProcessor.Object,
                    moverProcessor: mockMoverProcessor.Object);

        eventDispatcher = new EventDispatcher(effectDispatcher);
    }

    #region Events

    [Test]
    public void EventFilterCanBeModifiedAndAccessed()
    {
        eventDispatcher.Ignore(EventFilter.All);
        Assert.IsFalse(eventDispatcher.EventFilter.Allows(EventFilter.Melee));
        Assert.IsFalse(eventDispatcher.EventFilter.Allows(EventFilter.Attack));

        eventDispatcher.Allow(EventFilter.Melee);
        Assert.IsTrue(eventDispatcher.EventFilter.Allows(EventFilter.Melee));
        Assert.IsFalse(eventDispatcher.EventFilter.Allows(EventFilter.Attack));

        eventDispatcher.Allow(EventFilter.Attack);
        Assert.IsTrue(eventDispatcher.EventFilter.Allows(EventFilter.Melee));
        Assert.IsTrue(eventDispatcher.EventFilter.Allows(EventFilter.Attack));

        eventDispatcher.Ignore(EventFilter.Melee);
        Assert.IsFalse(eventDispatcher.EventFilter.Allows(EventFilter.Melee));
        Assert.IsTrue(eventDispatcher.EventFilter.Allows(EventFilter.Attack));
    }


    [Test]
    public void EventDispatcherFiltersEvents()
    {
        EventDispatcherStatus ret;
        IEvent e = meleeAttackWithDamage;

        // MeleeAttack Event processed IFF both flags are set
        eventDispatcher.Ignore(EventFilter.Melee | EventFilter.Attack);
        ret = eventDispatcher.DispatchEvent(e);
        Assert.IsTrue(ret.EventFiltered);

        eventDispatcher.Allow(EventFilter.Melee);
        ret = eventDispatcher.DispatchEvent(e);
        Assert.IsTrue(ret.EventFiltered);

        eventDispatcher.Allow(EventFilter.Attack);
        ret = eventDispatcher.DispatchEvent(e);
        Assert.IsTrue(ret.EventSucceeded);
    }


    [Test]
    public void EventDispatcherFiltersEffects()
    {
        EventDispatcherStatus ret;
        IEvent e = meleeAttackWithDamageAndKnockback;

        eventDispatcher.Allow(EventFilter.Melee | EventFilter.Attack);

        // Both effects allowed
        eventDispatcher.EffectDispatcher.Allow(EffectFilter.Damage | EffectFilter.Move);

        ret = eventDispatcher.DispatchEvent(e);
        Assert.IsTrue(ret.EventSucceeded);
        Assert.AreEqual(2, ret.EffectsSucceeded);
        Assert.AreEqual(0, ret.EffectsFiltered);
        Assert.AreEqual(0, ret.EffectsFailed);

        // Only 1 effect allowed
        eventDispatcher.EffectDispatcher.Allow(EffectFilter.Damage);
        eventDispatcher.EffectDispatcher.Ignore(EffectFilter.Move);

        ret = eventDispatcher.DispatchEvent(e);
        Assert.IsTrue(ret.EventSucceeded);
        Assert.AreEqual(1, ret.EffectsSucceeded);
        Assert.AreEqual(1, ret.EffectsFiltered);
        Assert.AreEqual(0, ret.EffectsFailed);

        // No effects allowed
        eventDispatcher.EffectDispatcher.Ignore(EffectFilter.Damage | EffectFilter.Move);

        ret = eventDispatcher.DispatchEvent(e);
        Assert.IsTrue(ret.EventSucceeded);
        Assert.AreEqual(0, ret.EffectsSucceeded);
        Assert.AreEqual(2, ret.EffectsFiltered);
        Assert.AreEqual(0, ret.EffectsFailed);
    }


    [Test]
    public void EventDispatcherInvokesEffectProcessors()
    {
        IEvent e = meleeAttackWithDamageAndKnockback;

        // Damage goes through but knockback does not
        eventDispatcher.Allow(EventFilter.Melee | EventFilter.Attack);
        eventDispatcher.EffectDispatcher.Allow(EffectFilter.Damage | EffectFilter.Move);

        eventDispatcher.DispatchEvent(e);

        mockHealthProcessor
            .Verify(p => p.ProcessEffect(It.IsAny<BaseEffect>()),
            Times.Exactly(1));

        mockMoverProcessor
            .Verify(p => p.ProcessEffect(It.IsAny<BaseEffect>()),
            Times.Exactly(1));
    }


    [Test]
    public void EventDispatcherProcessesAllEffects()
    {
        EventDispatcherStatus ret;
        IEvent e = meleeAttackWithDamageAndKnockback;

        // Damage goes through but knockback does not
        eventDispatcher.Allow(EventFilter.Melee | EventFilter.Attack);
        eventDispatcher.EffectDispatcher.Allow(EffectFilter.Damage | EffectFilter.Move);

        ret = eventDispatcher.DispatchEvent(e);
        Assert.IsTrue(ret.EventSucceeded);
        Assert.AreEqual(2, ret.EffectsSucceeded);
        Assert.AreEqual(0, ret.EffectsFiltered);
        Assert.AreEqual(0, ret.EffectsFailed);
    }

    #endregion

    #region Effects

    [Test]
    public void EffectFilterCanBeModifiedAndAccessed()
    {
        effectDispatcher.Ignore(EffectFilter.All);
        Assert.IsFalse(effectDispatcher.Allows(EffectFilter.Damage));
        Assert.IsFalse(effectDispatcher.Allows(EffectFilter.Move));

        effectDispatcher.Allow(EffectFilter.Damage);
        Assert.IsTrue(effectDispatcher.Allows(EffectFilter.Damage));
        Assert.IsFalse(effectDispatcher.Allows(EffectFilter.Move));

        effectDispatcher.Allow(EffectFilter.Move);
        Assert.IsTrue(effectDispatcher.Allows(EffectFilter.Damage));
        Assert.IsTrue(effectDispatcher.Allows(EffectFilter.Move));

        effectDispatcher.Ignore(EffectFilter.Damage);
        Assert.IsFalse(effectDispatcher.Allows(EffectFilter.Damage));
        Assert.IsTrue(effectDispatcher.Allows(EffectFilter.Move));
    }

    [Test]
    public void EffectDispatcherFailsWhenNoProcessorAvailable()
    {
        DispatchStatus[] ret;
        EffectDispatcher emptyEffectDispatcher = new EffectDispatcher();

        effectDispatcher.Allow(EffectFilter.Damage);
        effectDispatcher.Allow(EffectFilter.Move);

        ret = emptyEffectDispatcher.DispatchEffects(meleeAttackWithDamageAndKnockback);
        Assert.AreEqual(DispatchStatus.Failed, ret[0]);
        Assert.AreEqual(DispatchStatus.Failed, ret[1]);
    }


    [Test]
    public void EffectDispatcherFiltersEffects()
    {
        DispatchStatus[] ret;

        effectDispatcher.Allow(EffectFilter.Damage);
        effectDispatcher.Ignore(EffectFilter.Move);

        ret = effectDispatcher.DispatchEffects(meleeAttackWithDamageAndKnockback);
        Assert.AreEqual(DispatchStatus.Success, ret[0]);
        Assert.AreEqual(DispatchStatus.Filtered, ret[1]);
    }

    #endregion
}
