﻿namespace Robo.Component.Interface
{
    public interface IAmPrefab
    {
        string PrefabPath { get; }
    }
}