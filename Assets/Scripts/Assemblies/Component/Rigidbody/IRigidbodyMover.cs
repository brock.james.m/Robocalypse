using UnityEngine;

using Robo.Util.Vector;


namespace Robo.Component
{
    public interface IRigidbodyMover : IFacingDirection
    {
        float MoveSpeed { get; set; }
        void Move();

        bool Override { get; set; }
        Vector2 OverrideFacingDirection { get; set; }
        float OverrideMoveSpeed { get; set; }
    }
}