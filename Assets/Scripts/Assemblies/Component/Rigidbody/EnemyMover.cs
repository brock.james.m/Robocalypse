namespace Robo.Component
{
    public class EnemyMover : RigidbodyMover
    {
        #region IRigidbodyMover 

        public override void Move()
        {
            if (MoveSpeed > 0.0f && (FacingDirection.x != 0.0f || FacingDirection.y != 0.0f))
            {
                base.Move();
            }
        }

        #endregion
    }
}