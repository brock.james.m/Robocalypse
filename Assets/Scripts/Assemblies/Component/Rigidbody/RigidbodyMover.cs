using UnityEngine;

using Robo.Base;


namespace Robo.Component
{
    /// <summary>
    /// Moves a RigidBody on FixedUpdate
    /// </summary>
    public class RigidbodyMover : BaseComponent, IRigidbodyMover
    {
        protected Rigidbody2D rb2D;


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            this.rb2D = GetComponent<Rigidbody2D>();
        }

        protected override void FixedUpdate()
        {
            Move();
        }

        #endregion


        #region IRigidbodyMover

        /// <summary>
        /// Multipled by the MoveVector, indicates how far the rigidbody will move in one FixedUpdate
        /// </summary>
        public virtual float MoveSpeed
        {
            get => Override ? OverrideMoveSpeed : moveSpeed;
            set => moveSpeed = value;
        }
        [SerializeField] protected float moveSpeed = 0.0f;

        /// <summary>
        /// Desired move direction (normalized to 1).
        /// <para>
        /// Can be overriden
        /// </para>
        /// </summary>
        public virtual Vector2 FacingDirection
        {
            get => Override ? OverrideFacingDirection.normalized : moveVector;
            set => moveVector = value.normalized;
        }
        [SerializeField] protected Vector2 moveVector = Vector2.zero;

        /// <summary>
        /// Used in conjunction with OverrideFacingDirection and OverrideMoveSpeed
        /// </summary>
        /// <value></value>
        public bool Override { get; set; } = false;
        public virtual Vector2 OverrideFacingDirection { get; set; }
        public virtual float OverrideMoveSpeed { get; set; }

        public virtual void Move()
        {
            this.rb2D.position += (FacingDirection * MoveSpeed);
        }

        #endregion

    }
}