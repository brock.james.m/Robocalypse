namespace Robo.Component
{
    public class PlayerMover : RigidbodyMover
    {
        private int framewWithMoveInput = 0;


        #region IRigidbodyMover 

        public override void Move()
        {
            if (MoveSpeed > 0.0f && (FacingDirection.x != 0.0f || FacingDirection.y != 0.0f))
            {
                base.Move();

                framewWithMoveInput =
                    (FacingDirection.x != 0.0f || FacingDirection.y != 0.0f)
                        ? framewWithMoveInput + 1
                        : 0;
            }
        }

        #endregion
    }
}