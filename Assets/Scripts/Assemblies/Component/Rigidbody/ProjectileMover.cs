using UnityEngine;

using Robo.Util.Vector;


namespace Robo.Component
{
    /// <summary>
    /// Moves rigidbody regularly, faces rigidbody forward (towards current move vector)
    /// </summary>
    public class ProjectileMover : RigidbodyMover
    {
        #region IRigidbodyMover 

        public override void Move()
        {
            base.Move();

            this.rb2D.rotation = this.FacingDirection.DegreesClockwiseFromUp();
        }

        #endregion
    }
}