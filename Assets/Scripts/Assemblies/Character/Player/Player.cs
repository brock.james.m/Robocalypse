﻿using System;
using UnityEngine;

using Robo.Character.ActionHandler;
using Robo.Character.Player.Interface;
using Robo.Constants;
using Robo.Event;
using Robo.Event.Interface;
using Robo.Input;
using Robo.State;
using Robo.Util.UI;


namespace Robo.Character.Player
{
    public class Player : BaseCharacter, IPlayer
    {
        /* --- ICharacter --- */
        public override int MaxHealth => 1000;
        public override float MoveSpeed => 0.1f;


        /* --- IPlayer --- */
        public IUseDefaultActionMap ActionHandler { get; protected set; }
        public ICameraController CameraController { get; protected set; }
        public ICursor Cursor { get; protected set; }


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            CameraController = GameObject.Find(ObjName.MainCamera).GetComponent<CursorCamera>();
            Cursor = GameObject.Find(ObjName.Cursor).GetComponent<Robo.Input.Cursor>();

            HealthProcessor healthProcessor = new HealthProcessor(this);
            healthProcessor.OnHeal += this.OnHeal;
            healthProcessor.OnDamage += this.OnDamage;
            healthProcessor.OnDie += this.OnDie;

            MoverProcessor moverProcessor = new MoverProcessor(this);
            moverProcessor.OnCollide += this.OnCollide;

            this.EventDispatcher =
                new EventDispatcher(
                    new EffectDispatcher(
                        healthProcessor: healthProcessor,
                        moverProcessor: moverProcessor),
                    eventFilter: EventFilter.All);

            ActionHandler = new PlayerDefaultActionHandler(this);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            InputManager.SubscribeDefaultActionCallbacks(this.ActionHandler);

            InputManager.UseActionMap(InputManager.InputActions.Default);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            InputManager.UnsubscribeDefaultActionCallbacks(ActionHandler);
        }

        #endregion


        #region Events

        protected void OnHeal(int healing)
        {
            FloatingText.Show(
                healing.ToString(),
                spawnPosition: this.transform.position,
                color: Color.green,
                alphaMin: 0.3f,
                alphaMax: 1.0f,
                timeToLive: 2.0f);
        }

        protected void OnDamage(int damage)
        {
            FloatingText.Show(
                damage.ToString(),
                spawnPosition: this.transform.position,
                color: Color.red,
                alphaMin: 0.3f,
                alphaMax: 1.0f,
                timeToLive: 2.0f);
        }

        protected void OnDie()
        {
            FloatingText.Show(
                "ah fuk u got me",
                spawnPosition: this.transform.position,
                color: Color.white,
                alphaMin: 0.3f,
                alphaMax: 1.0f,
                timeToLive: 2.0f);

            Destroy(this.gameObject);
        }

        protected void OnCollide()
        {
            // throw new NotImplementedException();
        }

        #endregion


        #region State Machine

        protected override void SetupStateMachine()
        {
            // bad first example
            IState idleState = new IdleState();

            TransitionAnyWhen(IsIdle(), idleState);

            // void TransitionWhen(Func<bool> condition, IState from, IState to) =>
            //     stateMachine.AddTransition(from, to, condition);

            void TransitionAnyWhen(Func<bool> condition, IState to) =>
                stateMachine.AddAnyTransition(to, condition);

            Func<bool> IsIdle() => () => Mover.MoveSpeed == 0.0f;
        }

        #endregion

    }
}