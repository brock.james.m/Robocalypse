using Robo.Character.Interface;
using Robo.Input;


namespace Robo.Character.Player.Interface
{
    public interface IPlayer : ICharacter
    {
        IUseDefaultActionMap ActionHandler { get; }
        ICameraController CameraController { get; }
        ICursor Cursor { get; }
    }
}