using System;
using UnityEngine;

using Robo.Character.Interface;
using Robo.Component;
using Robo.Event.Component;
using Robo.State;


namespace Robo.Character
{
    public class BaseCharacter : EventBasedComponent, ICharacter
    {
        public virtual int MaxHealth { get; }
        public virtual float MoveSpeed { get; }

        public Animator Animator { get; protected set; }
        public SpriteRenderer SpriteRenderer { get; protected set; }
        public IRigidbodyMover Mover { get; protected set; }
        public IStateMachine stateMachine = new StateMachine();


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            SetupStateMachine();

            Animator = GetComponent<Animator>();
            SpriteRenderer = GetComponent<SpriteRenderer>();

            Mover = GetComponent<RigidbodyMover>();
            Mover.MoveSpeed = this.MoveSpeed;
        }

        #endregion

        protected virtual void SetupStateMachine()
        {
            throw new NotImplementedException();
        }
    }
}