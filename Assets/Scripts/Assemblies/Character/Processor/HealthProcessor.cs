using System;
using UnityEngine;

using Robo.Base;
using Robo.Character.Interface;
using Robo.Event;
using Robo.Event.Interface;
using Robo.Log;


namespace Robo.Character
{
    public class HealthProcessor : BaseObject, IEffectProcessor
    {
        public event Action OnDie;
        public event Action<int> OnDamage;
        public event Action<int> OnHeal;

        public int MaxHealth { get; private set; }
        protected int health = 0;
        public int Health
        {
            get => health;
            private set { health = Mathf.Clamp(value, 0, MaxHealth); if (health == 0) OnDie?.Invoke(); }
        }

        public HealthProcessor(ICharacter characterHealth)
        {
            this.MaxHealth = characterHealth.MaxHealth;
            this.Health = characterHealth.MaxHealth;
        }


        /* --- IEffectDispatcher --- */
        public DispatchStatus ProcessEffect(IEffect effect)
        {
            this.LogD($"HealthProcessor.ProcessEffect({effect.GetType().Name})");

            switch (effect)
            {
                case DamageEffect d:
                    return ProcessDamage(d.Damage);
                case HealEffect h:
                    return ProcessHealing(h.Healing);
                default:
                    return DispatchStatus.Failed;
            }
        }


        /* --- Helpers --- */
        protected virtual DispatchStatus ProcessDamage(int damage)
        {
            Health -= damage;
            OnDamage?.Invoke(damage);
            return DispatchStatus.Success;
        }

        protected virtual DispatchStatus ProcessHealing(int healing)
        {
            Health += healing;
            OnHeal?.Invoke(healing);
            return DispatchStatus.Success;
        }
    }
}