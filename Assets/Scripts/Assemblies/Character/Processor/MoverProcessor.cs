using System;
using System.Collections;
using UnityEngine;

using Robo.Base;
using Robo.Character.Interface;
using Robo.Event;
using Robo.Event.Interface;
using Robo.Util;
using Robo.Util.Vector;


namespace Robo.Character
{
    public class MoverProcessor : BaseObject, IEffectProcessor
    {
        public event Action OnCollide;

        protected BaseComponent component;
        protected ICharacter character;

        public MoverProcessor(BaseComponent component)
        {
            this.component = component;
            this.character = component as ICharacter;
        }


        /* --- IDashProcessor --- */
        public DispatchStatus ProcessEffect(IEffect effect)
        {
            component.StartCoroutine(Move(effect as IMoveEffect));

            return DispatchStatus.Success;
        }


        /* --- CoRoutines --- */
        protected IEnumerator Move(IMoveEffect moveEffect)
        {
            character.Mover.Override = true;

            Timer timer = new Timer(moveEffect.Duration, Time.time);
            while (timer.IsActive)
            {
                character.Mover.OverrideFacingDirection =
                    moveEffect.FacingDirection
                        .Rotate(moveEffect.RotateDegrees.FloatValueAt(timer.ElapsedPercent));

                character.Mover.OverrideMoveSpeed =
                    moveEffect.Speed.FloatValueAt(timer.ElapsedPercent);

                // TODO
                // if (collision detected)
                // {
                OnCollide?.Invoke();
                // }

                yield return null;
            }

            character.Mover.Override = false;
        }
    }
}