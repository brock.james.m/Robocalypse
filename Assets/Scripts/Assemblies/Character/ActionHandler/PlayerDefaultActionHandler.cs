using System;
using UnityEngine;
using UnityEngine.InputSystem;

using Robo.Base;
using Robo.Character.Player.Interface;
using Robo.Event;
using Robo.Event.Interface;
using Robo.Event.Component;
using Robo.Input;


namespace Robo.Character.ActionHandler
{
    public class PlayerDefaultActionHandler : BaseObject, IUseDefaultActionMap
    {
        protected Transform playerPosition;
        protected IPlayer player;
        protected IEventDispatcher eventDispatcher;


        public PlayerDefaultActionHandler(EventBasedComponent player)
        {
            this.playerPosition = player.transform;
            this.player = player as IPlayer;
            this.eventDispatcher = player.EventDispatcher;
        }


        /* --- IUseDefaultActionMap --- */
        public void OnMove(InputAction.CallbackContext ctx)
        {
            player.Mover.FacingDirection = ctx.ReadValue<Vector2>();

            // TODO: move to interfaced components?
            player.Animator.SetFloat("MoveSpeedX", player.Mover.FacingDirection.x);
            player.Animator.SetFloat("MoveSpeedY", player.Mover.FacingDirection.y);
            player.Animator.SetFloat("MoveMagnitude", player.Mover.FacingDirection.magnitude);
            player.SpriteRenderer.flipX = (player.Mover.FacingDirection.x < 0.0f);
        }

        public void OnMoveCancel(InputAction.CallbackContext ctx)
        {
            player.Mover.FacingDirection = Vector2.zero;

            // TODO: move to interfaced components?
            player.Animator.SetFloat("MoveSpeedX", player.Mover.FacingDirection.x);
            player.Animator.SetFloat("MoveSpeedY", player.Mover.FacingDirection.y);
            player.Animator.SetFloat("MoveMagnitude", player.Mover.FacingDirection.magnitude);
            player.SpriteRenderer.flipX = (player.Mover.FacingDirection.x < 0.0f);
        }

        public void OnDash(InputAction.CallbackContext ctx)
        {
            IActionEvent dashEvent =
                new ActionEventBuilder().Builder()
                    .WithEffect(
                        new MoveEffect(
                            duration: 0.2f,
                            speed: MoveEffect.DashSpeed(0.2f, 0.01f, 0.0f))
                    )
                    .Facing(player.Mover.FacingDirection)
                .Build();

            eventDispatcher.DispatchEvent(dashEvent);
        }

        public void OnAttack(InputAction.CallbackContext ctx)
        {
            // TODO:
        }

        public void OnButtonEast(InputAction.CallbackContext ctx)
        {
            // TODO
        }

        public void OnFocus(InputAction.CallbackContext ctx)
        {
            player.Cursor.Mode = Robo.Input.CursorMode.Focus;
        }

        public void OnFocusCancel(InputAction.CallbackContext ctx)
        {
            player.Cursor.Mode = Robo.Input.CursorMode.Aim;
        }

        public void OnAbility(InputAction.CallbackContext ctx)
        {
            System.Random rand = new System.Random();

            float timeToLive = 1.0f;
            float speed = 0.40f;

            int damage = 50 + rand.Next(-4, 4);
            float knockbackDuration = 0.04f;
            float knockbackSpeed = 0.2f;

            LightningBolt.Create(
                player.Cursor.ProjectileSpawnPosition,
                player.Cursor.Aim,
                timeToLive,
                speed,
                damage,
                knockbackDuration,
                knockbackSpeed);
        }

        public void OnViewMenu(InputAction.CallbackContext ctx)
        {
            // BaseMenu.CreateMenuFromPrefab(playerPosition.parent.gameObject, ViewMenu.PrefabPath);
        }

        public void OnGameMenu(InputAction.CallbackContext ctx)
        {
            throw new NotImplementedException();
        }

        public void OnCameraZoom(InputAction.CallbackContext ctx)
        {
            Vector2 scroll = ctx.ReadValue<Vector2>();

            if (scroll.y < 0)
            {
                player.CameraController.ZoomOut();
            }
            else if (scroll.y > 0)
            {
                player.CameraController.ZoomIn();
            }
        }

        public void OnDebugLevelDown(InputAction.CallbackContext ctx)
        {
            Robo.Log.Logger.DecreaseLogLevel();
        }

        public void OnDebugLevelUp(InputAction.CallbackContext ctx)
        {
            Robo.Log.Logger.IncreaseLogLevel();
        }
    }
}