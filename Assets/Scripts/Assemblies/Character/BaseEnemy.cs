using UnityEngine;

using Robo.Event;
using Robo.Event.Interface;
using Robo.State;
using Robo.Util.UI;


namespace Robo.Character
{
    public class BaseEnemy : BaseCharacter
    {
        /* --- ICharacter --- */
        public override int MaxHealth => 1000;
        public override float MoveSpeed => 0.0f;


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            HealthProcessor healthProcessor = new HealthProcessor(this);
            healthProcessor.OnHeal += this.OnHeal;
            healthProcessor.OnDamage += this.OnDamage;
            healthProcessor.OnDie += this.OnDie;

            MoverProcessor moverProcessor = new MoverProcessor(this);
            moverProcessor.OnCollide += this.OnCollide;

            this.EventDispatcher =
                new EventDispatcher(
                    new EffectDispatcher(
                        healthProcessor: healthProcessor,
                        moverProcessor: moverProcessor),
                    eventFilter: EventFilter.All);
        }

        #endregion


        #region Events

        protected void OnHeal(int healing)
        {
            FloatingText.Show(
                healing.ToString(),
                spawnPosition: this.transform.position,
                color: Color.green,
                alphaMin: 0.3f,
                alphaMax: 1.0f,
                timeToLive: 2.0f);
        }

        protected void OnDamage(int damage)
        {
            FloatingText.Show(
                damage.ToString(),
                spawnPosition: this.transform.position,
                color: Color.red,
                alphaMin: 0.3f,
                alphaMax: 1.0f,
                timeToLive: 2.0f);
        }

        protected void OnDie()
        {
            FloatingText.Show(
                "ah fuck u got me",
                spawnPosition: this.transform.position,
                color: Color.white,
                alphaMin: 0.3f,
                alphaMax: 1.0f,
                timeToLive: 2.0f);

            Destroy(this.gameObject);
        }

        protected void OnCollide()
        {
            //throw new NotImplementedException();
        }

        #endregion


        #region State Machine

        protected override void SetupStateMachine()
        {
            // idlestate is for player -- need base state for enemy
            // // bad first example
            // IState idleState = new IdleState();

            // TransitionAnyWhen(IsIdle(), idleState);

            // void TransitionWhen(Func<bool> condition, IState from, IState to) =>
            //     stateMachine.AddTransition(from, to, condition);

            // void TransitionAnyWhen(Func<bool> condition, IState to) =>
            //     stateMachine.AddAnyTransition(to, condition);

            // Func<bool> IsIdle() => () => Mover.MoveSpeed == 0.0f;
        }

        #endregion
    }
}