using UnityEngine;

using Robo.Component;


namespace Robo.Character.Interface
{
    public interface ICharacter
    {
        Animator Animator { get; }
        SpriteRenderer SpriteRenderer { get; }
        IRigidbodyMover Mover { get; }
        int MaxHealth { get; }
        float MoveSpeed { get; }
    }
}