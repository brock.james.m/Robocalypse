using UnityEngine;


namespace Robo.Input
{
    public class StaticFollowCamera : BaseCameraController
    {

        /* --- ICameraController --- */
        public override Vector3 CameraPosition => this.transform.position;
        public override Vector3 CameraRotation => this.transform.rotation.eulerAngles;

        public override void ZoomIn()
        {
        }

        public override void ZoomOut()
        {
        }
    }
}