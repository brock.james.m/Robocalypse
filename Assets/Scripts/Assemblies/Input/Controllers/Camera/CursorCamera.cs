using UnityEngine;

using Robo.Constants;


namespace Robo.Input
{
    public interface ILeashedPosition
    {

    }

    /// <summary>
    /// Controller class for Perspective camera looking on layered tilemaps
    /// </summary>
    public class CursorCamera : BaseCameraController
    {
        /// <summary>
        /// Cursor
        /// </summary>
        public ICursor Cursor;

        /// <summary>
        ///  Max distance the camera can be pulled away from the player by the cursor
        /// </summary>
        public float CameraMaxLeash = 8.0f;

        /// <summary>
        /// How far the camera will travel based on the distance between the gameObject and the cursor
        /// </summary>
        public float CameraLeashFactor = 0.2f;

        /// <summary>
        ///  Max 'angle'' the camera can be rotated away from the default top-down position
        /// </summary>
        public Vector2 CameraMaxRotateLeash = new Vector2(x: 1.0f, y: 1.0f);


        /// <summary>
        /// |Z-axis value| of the camera object
        /// <para>
        /// CameraHeight works in positive values, even though the transform.z may be < 0.0f
        /// </summary>
        public override float CameraHeight
        {
            get => cameraHeight;
            protected set { cameraHeight = Mathf.Clamp(value, CameraMinHeight, CameraMaxHeight); }
        }
        [SerializeField]
        protected float cameraHeight = 10.0f;

        public float CameraMinHeight = 6.0f;
        public float CameraMaxHeight = 12.0f;
        public float HeightRange => CameraMaxHeight - CameraMinHeight;
        public float HeightPercent => (CameraHeight - CameraMinHeight) / HeightRange;

        protected float ZoomIncrement => (0.3f + 0.2f * (1.0f - HeightPercent));
        protected Vector2 ZoomPan => new Vector2(0.0f, HeightPercent - 1.0f);

        #region Leash

        /// <summary>
        /// Transform that allows a Leash of some radius around it
        /// </summary>
        protected Transform LeashHolder;

        /// <summary>
        /// Camera is leashed to player Leash Distance
        /// </summary>
        protected Vector2 Leash
        {
            get
            {
                Leash = Cursor.Aim * CameraLeashFactor;
                return leash;
            }
            set
            {
                Vector2 newLeash = new Vector2(
                    Mathf.Clamp(value.x, -CameraMaxLeash, CameraMaxLeash),
                    Mathf.Clamp(value.y, -CameraMaxLeash, CameraMaxLeash)
                );

                leash = newLeash;
            }
        }
        [SerializeField]
        protected Vector2 leash;

        #endregion

        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            GameObject cursorObj = GameObject.Find(ObjName.Cursor);
            Cursor = cursorObj.GetComponent<Cursor>();
            LeashHolder = cursorObj.transform.parent.transform;
        }

        #endregion

        #region  ICameraController

        public override void ZoomIn()
        {
            CameraHeight -= ZoomIncrement;
        }

        public override void ZoomOut()
        {
            CameraHeight += ZoomIncrement;
        }

        #endregion

        #region  BaseCameraController

        /// <summary>
        /// Position affected by cursor leash and Zoom level
        /// </summary>
        protected override Vector3 ExpectedCameraPosition
        {
            get =>
                new Vector3(
                    LeashHolder.position.x + Leash.x + ZoomPan.x,
                    LeashHolder.position.y + Leash.y + ZoomPan.y,
                    -CameraHeight);
        }

        /// <summary>
        /// Rotation affected by cursor leash and Zoom level
        /// </summary>
        /// <value></value>
        protected override Vector3 ExpectedCameraRotation
        {
            get =>
                // Mouse X-axis = Unity Y-axis
                // Mouse Y-axis = Unity X-axis
                new Vector3(
                    ZoomPan.y * 8.0f, // Level out camera as we zoom in
                    0.0f,
                    0.0f
                );
        }

        #endregion
    }
}