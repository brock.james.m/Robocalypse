using UnityEngine;


namespace Robo.Input
{
    public interface ICameraController
    {
        float CameraHeight { get; }
        Vector3 CameraPosition { get; }
        Vector3 CameraRotation { get; }

        void ZoomIn();
        void ZoomOut();
    }
}