using System;
using UnityEngine;

using Robo.Base;
using Robo.Constants;
using Robo.Log;
using Robo.Util.Vector;


namespace Robo.Input
{
    /// <summary>
    /// Component that sets the transform position and rotation of the MainCamera
    /// </summary>
    public class BaseCameraController : BaseComponent, ICameraController
    {
        protected Camera cam;
        public float CamTravelTimePercent => Mathf.Clamp((float)framesWithTravel / maxFramesWithTravel, 0.0f, 1.0f);
        public float CamRotateTimePercent => Mathf.Clamp((float)framesWithTravel / maxFramesWithRotate, 0.0f, 1.0f);
        protected int framesWithTravel = 0;
        public int maxFramesWithTravel = 150;
        public int maxFramesWithRotate = 30;

        /// <summary>
        /// Subclasses tell base where to go. Base moves it.
        /// </summary>
        protected virtual Vector3 ExpectedCameraPosition { get; }
        private Vector3 targetCameraPosition;
        private Vector3 targetCameraRotation;

        /// <summary>
        /// Subclasses tell base how to rotate. Base rotates it.
        /// </summary>
        protected virtual Vector3 ExpectedCameraRotation { get; }


        #region Lifecycle
        protected override void Awake()
        {
            base.Awake();

            this.cam = Camera.main;

            this.cameraPosition = this.transform.position;
            this.cameraRotation = this.transform.rotation.eulerAngles;
        }

        protected override void Update()
        {
            base.Update();

            // Expected position for CursorCamera would be some place between Cursor and Camera
            targetCameraPosition = ExpectedCameraPosition;
            targetCameraRotation = ExpectedCameraRotation;

            // Do nothing if we are already in the expected position
            if (framesWithTravel > 0 && CameraPosition.RoughlyEquals(targetCameraPosition))
            {
                CameraPosition = targetCameraPosition;
                CameraRotation = targetCameraRotation;
                framesWithTravel = 0;
                return;
            }

            CameraPosition = Vector3.Lerp(CameraPosition, targetCameraPosition, CamTravelTimePercent);
            cam.transform.position = CameraPosition;

            //CameraRotation = Vector3.Lerp(CameraRotation, targetCameraRotation, CamRotateTimePercent);
            //cam.transform.eulerAngles = CameraRotation;

            framesWithTravel++;
        }

        #endregion

        #region ICameraController

        public virtual float CameraHeight { get; protected set; }
        public virtual Vector3 CameraPosition
        {
            get => cameraPosition;
            private set => cameraPosition = value;
        }
        protected Vector3 cameraPosition;
        public virtual Vector3 CameraRotation
        {
            get => cameraRotation;
            private set => cameraRotation = value;
        }
        protected Vector3 cameraRotation;


        public virtual void ZoomIn()
        {
            throw new NotImplementedException();
        }

        public virtual void ZoomOut()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}