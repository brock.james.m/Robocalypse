using UnityEngine;


namespace Robo.Input
{
    public interface ICursor
    {
        /// <summary>
        /// Display mode (which anim)
        /// </summary>
        CursorMode Mode { get; set; }

        /// <summary>
        /// Where to show Cursor.
        /// </summary>
        Vector3 Position { get; }

        /// <summary>
        /// The direction from the parent to the Cursor.
        /// </summary>
        Vector2 Aim { get; }

        /// <summary>
        /// Where to spawn projectiles.
        /// <para>
        /// Equal to Aim normalized to some value.
        /// </para>
        /// </summary>
        Vector3 ProjectileSpawnPosition { get; }

        /// <summary>
        /// Set Position, Aim, and SpawnPosition from Unity Input Cursor screen position.
        /// </summary>
        void SetFromMousePosition(Vector2 mousePosition);

        /// <summary>
        /// Set Position, Aim, and SpawnPosition from Unity Input Gamepad aim vector.
        /// </summary>
        void SetFromGamepadAim(Vector2 gamepadAim);
    }
}