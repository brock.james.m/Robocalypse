using System;
using UnityEngine;

using Robo.Base;
using Robo.Constants;
using Robo.Log;
using Robo.Util.Vector;


namespace Robo.Input
{
    public enum CursorMode
    {
        Aim = 0,
        Focus
    }

    /// <summary>
    /// Represents Aim. 
    /// <para>
    /// Anchored to parent transform.
    /// </para>
    /// </summary>
    public class Cursor : BaseComponent, ICursor
    {
        [SerializeField]
        protected CursorMode mode = CursorMode.Aim;
        public CursorMode Mode
        {
            get => mode;
            set
            {
                mode = value;
                animator.SetInteger("CursorMode", (int)mode);
            }
        }

        protected Transform holder;
        protected ICameraController cameraController;
        protected Animator animator;


        // Adjust everything down a bit to compensate for camera angle
        protected float yOffset = -0.3f;


        #region ICursor

        public Vector3 Position
        {
            get => position;
            protected set
            {
                position =
                    value.With(
                        y: value.y + yOffset,
                        z: -1);
            }
        }
        [SerializeField] protected Vector3 position = Vector3.zero;


        public Vector2 Aim
        {
            get => aim;
            protected set
            {
                aim = value.LimitDistance(MaxAimDistance);
                ProjectileSpawnPosition = value.With(z: this.transform.position.z);
            }
        }
        [SerializeField] protected Vector2 aim = Vector3.zero;
        public float MaxAimDistance = 1.4f;


        public Vector3 ProjectileSpawnPosition
        {
            get => projectilSpawnPosition;
            protected set
            {
                projectilSpawnPosition =
                    (holder.position + value.Normalize(ProjectileSpawnDistance));

                projectilSpawnPosition.y += yOffset;
            }
        }
        [SerializeField] protected Vector3 projectilSpawnPosition = Vector3.zero;
        public float ProjectileSpawnDistance = 1.0f;


        public void SetFromMousePosition(Vector2 mousePosition)
        {
            Vector3 cursorWorldPosition =
                Camera.main.ScreenToWorldPoint(
                    mousePosition.With(z: cameraController.CameraHeight));

            this.Position = cursorWorldPosition;
            this.Aim = holder.position.To(this.Position);
        }

        public void SetFromGamepadAim(Vector2 aim)
        {
            this.Aim = aim;
            this.Position = holder.position + Aim.With(z: 0);
        }

        #endregion


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            holder = this.transform.parent.transform;

            cameraController = GameObject.Find(ObjName.MainCamera).GetComponent<BaseCameraController>();
            animator = GetComponent<Animator>();

        }
        protected override void Update()
        {
            base.Update();

            switch (InputManager.CurrentDevice)
            {
                // CursorAim is set directly. Add Aim to parent to get position.
                case CurrentDevice.Gamepad:
                    SetFromGamepadAim(
                        InputManager.InputActions.Default.GamepadAim.ReadValue<Vector2>()
                    );
                    break;

                // Set CursorPosition. Compute Aim.
                case CurrentDevice.KeyboardAndMouse:
                    SetFromMousePosition(
                        InputManager.InputActions.Default.CursorPosition.ReadValue<Vector2>()
                    );
                    break;

                default:
                    if (logCurrentDeviceUnknown == 0)
                    {
                        this.LogW($"Device {InputManager.CurrentDevice} not supported!");
                        throw new NotImplementedException();
                    }
                    break;
            }

            this.transform.position = Position;
            this.transform.eulerAngles = Vector2.zero.With(z: Aim.DegreesClockwiseFromUp() - 90.0f);
        }

        protected static int logCurrentDeviceUnknown = 0;

        #endregion
    }
}