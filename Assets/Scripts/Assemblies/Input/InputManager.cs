using System;
using UnityEngine.InputSystem;

using Robo.Base;
using Robo.Input;
using Robo.Log;


namespace Robo.Input
{
    public enum CurrentDevice
    {
        None = 0,
        KeyboardAndMouse,
        Gamepad
    }

    public class InputManager : BaseComponent
    {
        public static string StaticLogTag => "InputManager";

        /// <summary>
        /// Which input device is currently being used.
        /// <para>
        /// When subscribing to InputActions, IUseActionMap object should 
        /// subscribe GetCurrentInputDevice to a move/aim function
        /// </para>
        /// </summary>
        public static CurrentDevice CurrentDevice
        {
            get => currentDevice;
            set
            {
                if (currentDevice != value)
                {
                    currentDevice = value;
                    ILoggableExtensions.LogI(StaticLogTag, $"Switching to input from '{currentDevice}'");
                }
            }
        }
        protected static CurrentDevice currentDevice = CurrentDevice.None;

        public static PlayerInputActions InputActions;
        public static event Action<InputActionMap> actionMapChange;

        private static InputActionMap curActionMap;
        private static InputActionMap prevActionMap;


        /* --- Public API --- */
        public static void UseActionMap(InputActionMap actionMap)
        {
            if (actionMap.enabled)
            {
                return;
            }

            ILoggableExtensions.LogI(StaticLogTag, $"UseActionMap({actionMap.name})");

            prevActionMap = curActionMap;
            curActionMap = actionMap;

            // Disable all, enable chosen
            InputActions.Disable();
            curActionMap.Enable();

            actionMapChange?.Invoke(curActionMap);
        }

        public static void UsePrevActionMap()
        {
            UseActionMap(prevActionMap);
        }

        public static void SubscribeDefaultActionCallbacks(IUseDefaultActionMap user)
        {
            if (InputActions == null)
            {
                ILoggableExtensions.LogW(StaticLogTag, $"InputActions is null! Check ScriptExecutionOrder");
            }

            // Check for (and set) CurrentDevice on move or aim
            InputActions.Default.Move.performed += GetCurrentInputDevice;
            InputActions.Default.GamepadAim.performed += GetCurrentInputDevice;

            InputActions.Default.Move.performed += user.OnMove;
            InputActions.Default.Move.canceled += user.OnMoveCancel;

            InputActions.Default.Dash.performed += user.OnDash;
            InputActions.Default.Attack.performed += user.OnAttack;
            InputActions.Default.TempButtonEast.performed += user.OnButtonEast;

            InputActions.Default.Focus.performed += user.OnFocus;
            InputActions.Default.Focus.canceled += user.OnFocusCancel;
            InputActions.Default.Ability.performed += user.OnAbility;

            InputActions.Default.ViewMenu.performed += user.OnViewMenu;
            InputActions.Default.GameMenu.performed += user.OnGameMenu;

            InputActions.Default.CameraZoom.performed += user.OnCameraZoom;

            InputActions.Default.DebugLevelDown.performed += user.OnDebugLevelDown;
            InputActions.Default.DebugLevelUp.performed += user.OnDebugLevelUp;
        }

        public static void UnsubscribeDefaultActionCallbacks(IUseDefaultActionMap user)
        {
            InputActions.Default.Move.performed -= GetCurrentInputDevice;
            InputActions.Default.GamepadAim.performed -= GetCurrentInputDevice;

            InputActions.Default.Move.performed -= user.OnMove;
            InputActions.Default.Move.canceled -= user.OnMoveCancel;

            InputActions.Default.Dash.performed -= user.OnDash;
            InputActions.Default.Attack.performed -= user.OnAttack;
            InputActions.Default.TempButtonEast.performed -= user.OnButtonEast;

            InputActions.Default.Focus.performed -= user.OnFocus;
            InputActions.Default.Focus.canceled -= user.OnFocusCancel;
            InputActions.Default.Ability.performed -= user.OnAbility;

            InputActions.Default.ViewMenu.performed -= user.OnViewMenu;
            InputActions.Default.GameMenu.performed -= user.OnGameMenu;

            InputActions.Default.CameraZoom.performed -= user.OnCameraZoom;

            InputActions.Default.DebugLevelDown.performed -= user.OnDebugLevelDown;
            InputActions.Default.DebugLevelUp.performed -= user.OnDebugLevelUp;
        }


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            InputActions = new PlayerInputActions();
            InputActions.Disable();

            curActionMap = InputActions.Default;
            UseActionMap(InputActions.Default);

            InputSystem.onDeviceChange += OnDeviceChange;
        }

        #endregion


        /// <summary>
        ///  https://docs.unity3d.com/Packages/com.unity.inputsystem@1.0/api/UnityEngine.InputSystem.InputDeviceChange.html
        /// </summary>
        /// <param name="device"></param>
        /// <param name="deviceChange"></param>
        protected void OnDeviceChange(InputDevice device, InputDeviceChange deviceChange)
        {
            this.LogI(
                $"'{device}' {deviceChange}: id ({device.deviceId}), enabled ({device.enabled})\n"
            );

            switch (deviceChange)
            {
                case InputDeviceChange.Added:
                    break;
                case InputDeviceChange.Removed:
                    break;
                case InputDeviceChange.Disconnected:
                    break;
                case InputDeviceChange.Reconnected:
                    break;
                case InputDeviceChange.Enabled:
                    break;
                case InputDeviceChange.Disabled:
                    break;
                case InputDeviceChange.Destroyed:
                    break;
                default:
                    // See InputDeviceChange reference for other event types.
                    break;
            }
        }

        protected static void GetCurrentInputDevice(InputAction.CallbackContext ctx)
        {
            switch (ctx.control.device)
            {
                case Keyboard keyboard:
                    CurrentDevice = CurrentDevice.KeyboardAndMouse;
                    return;
                case Gamepad gamepad:
                    CurrentDevice = CurrentDevice.Gamepad;
                    return;
            }
        }
    }
}