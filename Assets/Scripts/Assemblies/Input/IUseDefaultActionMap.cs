using UnityEngine.InputSystem;


namespace Robo.Input
{
    public interface IUseDefaultActionMap
    {
        void OnMove(InputAction.CallbackContext ctx);
        void OnMoveCancel(InputAction.CallbackContext ctx);
        void OnFocus(InputAction.CallbackContext ctx);
        void OnFocusCancel(InputAction.CallbackContext ctx);
        void OnDash(InputAction.CallbackContext ctx);
        void OnAttack(InputAction.CallbackContext ctx);
        void OnButtonEast(InputAction.CallbackContext ctx);
        void OnAbility(InputAction.CallbackContext ctx);
        void OnViewMenu(InputAction.CallbackContext ctx);
        void OnGameMenu(InputAction.CallbackContext ctx);
        void OnCameraZoom(InputAction.CallbackContext ctx);
        void OnDebugLevelDown(InputAction.CallbackContext ctx);
        void OnDebugLevelUp(InputAction.CallbackContext ctx);
    }
}