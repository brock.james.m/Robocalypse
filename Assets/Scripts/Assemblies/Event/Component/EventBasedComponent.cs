using Robo.Base;
using Robo.Event.Interface;


namespace Robo.Event.Component
{
    /// <summary>
    /// Base class for anything that wants to handle (receive) Events
    /// </summary>
    public class EventBasedComponent : BaseComponent
    {
        public IEventDispatcher EventDispatcher = null;
    }
}