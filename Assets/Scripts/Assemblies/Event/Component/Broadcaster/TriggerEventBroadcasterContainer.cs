using UnityEngine;

using Robo.Base;
using Robo.Event.Interface;
using Robo.Event.Interface.Trigger;


namespace Robo.Event.Component
{
    /// <summary>
    /// Base class for anything that wants to send Events when the collider invokes OnTrigger callbacks.
    /// </summary>
    public class TriggerEventBroadcasterContainer : BaseComponent
    {
        public ITriggerEvent TriggerEvent { get; set; }
        public ITriggerEventBroadcaster EventBroadcaster;

        protected Collider2D triggerHitbox;


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            triggerHitbox = GetComponent<Collider2D>();

            EventBroadcaster = new TriggerEventBroadcaster();
        }

        #endregion


        #region Trigger

        protected virtual void OnTriggerEnter2D(Collider2D collider)
        {
            IEventDispatcher eventDispatcher =
                collider.gameObject
                .GetComponent<EventBasedComponent>()
                ?.EventDispatcher;

            EventBroadcaster.BroadcastTo(
                collider.gameObject,
                eventDispatcher,
                this.TriggerEvent);
        }

        #endregion
    }
}