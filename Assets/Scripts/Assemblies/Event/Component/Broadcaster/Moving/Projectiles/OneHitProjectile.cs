using UnityEngine;

using Robo.Component;
using Robo.Event.Interface;


namespace Robo.Event.Component
{
    /// <summary>
    /// Projectile that despawns after its first collision
    /// </summary>
    public class OneHitProjectile : BaseProjectile
    {
        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            EventBroadcaster.OnTriggerSelf += DestroyProjectile;

            mover = GetComponent<ProjectileMover>();
        }

        #endregion

        #region EventBroadcaster

        public void DestroyProjectile()
        {
            Destroy(this.gameObject);
        }

        #endregion
    }
}