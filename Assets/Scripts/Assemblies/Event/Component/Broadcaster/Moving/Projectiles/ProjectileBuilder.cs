using UnityEngine;

using Robo.Event.Interface;


namespace Robo.Event.Component
{
    public class ProjectileBuilder<T> where T : BaseProjectile
    {
        protected static ProjectileBuilder<T> me;
        protected T projectile;

        protected ProjectileBuilder(T projectile)
        {
            this.projectile = projectile;
        }

        public static ProjectileBuilder<T> Builder(GameObject projectileObj)
        {
            T projectile = projectileObj.AddComponent<T>();

            me = new ProjectileBuilder<T>(projectile);
            return me;
        }

        public ProjectileBuilder<T> Facing(Vector3 facing)
        {
            projectile.SpawnFacing = facing;
            return me;
        }

        public ProjectileBuilder<T> WithSpeed(float moveSpeed)
        {
            projectile.MoveSpeed = moveSpeed;
            return me;
        }

        public ProjectileBuilder<T> WithTTL(float ttl)
        {
            projectile.TimeToLive = ttl;
            return me;
        }

        public ProjectileBuilder<T> WithTriggerEvent(ITriggerEvent e)
        {
            projectile.TriggerEvent = e;
            return me;
        }

        public T Build()
        {
            return projectile;
        }
    }
}