using Robo.Event.Interface;
using Robo.Util;


namespace Robo.Event.Component
{
    public interface IAbility
    {
        Timer CooldownTimer { get; }
        ITriggerEvent TriggerEvent { get; }
    }
}