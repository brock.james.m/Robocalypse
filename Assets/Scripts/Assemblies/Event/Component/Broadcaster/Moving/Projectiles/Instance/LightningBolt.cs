using System.Collections.Generic;
using UnityEngine;

using Robo.Constants;
using Robo.Event.Interface;
using Robo.Util;


namespace Robo.Event.Component
{
    /// <summary>
    /// Test ability: FORM OF... LIGHTNING BOLT!!!
    /// </summary>
    public class LightningBolt : Ability
    {
        private float cooldown = 1.0f;

        /* --- IAbility --- */
        /// <summary>
        /// Timer that controls cooldowns for all LightningBolts.
        /// </summary>
        public override Timer CooldownTimer
        {
            get
            {
                if (lightningBoltCooldownTimer == null)
                {
                    // First time, init new timer as off cooldown
                    lightningBoltCooldownTimer = new Timer(cooldown);
                }
                return lightningBoltCooldownTimer;
            }
        }
        private static Timer lightningBoltCooldownTimer;

        public override ITriggerEvent TriggerEvent { get; }


        protected static string prefabPath = Constants.PrefabPath.LightningBolt;
        public override List<string> IgnoredTags =>
            new List<string>()
            {
                ObjTag.PassableTerrain,
                ObjTag.Player,
                ObjTag.FriendlyProjectile,
                ObjTag.Collectible
            };

        public static void Create(
            Vector2 spawnPosition,
            Vector2 facing,
            float timeToLive,
            float speed,
            int damage,
            float knockbackDuration,
            float knockbackSpeed)
        {
            LightningBolt lightningBolt =
                new LightningBolt(
                    damage,
                    knockbackDuration,
                    knockbackSpeed);

            if (!lightningBolt.isActive)
            {
                return;
            }

            GameObject lightningBoltObj = Prefab.Instantiate(prefabPath);
            lightningBoltObj.transform.position = spawnPosition;

            ProjectileBuilder<OneHitProjectile>.Builder(lightningBoltObj)
                .Facing(facing)
                .WithSpeed(speed)
                .WithTTL(timeToLive)
                .WithTriggerEvent(lightningBolt.TriggerEvent)
            .Build();
        }

        protected LightningBolt(
            int damage,
            float knockbackDuration,
            float knockbackSpeed)
        {
            this.TriggerEvent =
                new TriggerEventBuilder().Builder()
                    .WithIgnoredTags(this.IgnoredTags)
                    .WithEffect(new DamageEffect(damage))
                    .WithEffect(
                        new MoveEffect(
                            duration: knockbackDuration,
                            speed: MoveEffect.KnockbackSpeed(knockbackSpeed)))
                .Build();
        }
    }
}