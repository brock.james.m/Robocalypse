using UnityEngine;

using Robo.Component;


namespace Robo.Event.Component
{
    public class BaseProjectile : MovingTriggerEventBroadcasterContainer
    {
        /// <summary>
        /// Setting this value also rotates the transform accordingly
        /// </summary>
        /// <value></value>
        public Vector2 SpawnFacing
        {
            get => spawnFacing;
            set
            {
                spawnFacing = value;

                mover.FacingDirection = spawnFacing;

                this.transform.rotation =
                    Quaternion.Euler(
                        Vector3.forward
                        * Mathf.Atan2(spawnFacing.y, spawnFacing.x)
                        * Mathf.Rad2Deg);
            }
        }
        protected Vector2 spawnFacing;

        /// <summary>
        /// Setting this value also sets the mover's speed
        /// </summary>
        public float MoveSpeed
        {
            get => moveSpeed;
            set { moveSpeed = value; mover.MoveSpeed = moveSpeed; }
        }
        private float moveSpeed;

        public float TimeToLive;


        #region Lifecycle

        protected override void Update()
        {
            base.Update();

            TimeToLive -= Time.deltaTime;
            if (TimeToLive <= 0)
            {
                Destroy(gameObject);
            }
        }

        #endregion
    }
}