using System.Collections.Generic;
using UnityEngine;

using Robo.Component.Interface;
using Robo.Event.Interface;
using Robo.Util;


namespace Robo.Event.Component
{
    public class Ability : IAbility, IIgnoreTags, IAmPrefab
    {
        /* --- IAbility --- */
        /// <summary>
        /// Reference to a timer that subclasses should set (static?)
        /// </summary>
        public virtual Timer CooldownTimer { get; protected set; }

        public virtual ITriggerEvent TriggerEvent => throw new System.NotImplementedException();


        /* --- IIgnoreTags --- */
        public virtual List<string> IgnoredTags { get; set; }


        /* --- IAmPrefab --- */
        public virtual string PrefabPath => throw new System.NotImplementedException();


        protected bool isActive = false;


        /// <summary>
        /// Constructor
        /// <para>
        /// When a subclass is instantiated, check the cooldown timer.
        /// </para>
        /// </summary>
        protected Ability()
        {
            if (CooldownTimer.IsActive)
            {
                return;
            }

            isActive = true;
            CooldownTimer.Reset(Time.time);
        }
    }
}