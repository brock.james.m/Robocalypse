using UnityEngine;

using Robo.Component;


namespace Robo.Event.Component
{
    public class MovingTriggerEventBroadcasterContainer : TriggerEventBroadcasterContainer
    {
        protected IRigidbodyMover mover;


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            triggerHitbox = GetComponent<Collider2D>();

            EventBroadcaster = new TriggerEventBroadcaster();
            mover = GetComponent<RigidbodyMover>();
        }

        #endregion

        #region Trigger

        protected override void OnTriggerEnter2D(Collider2D collider)
        {
            this.TriggerEvent.FacingDirection = mover.FacingDirection;

            base.OnTriggerEnter2D(collider);
        }

        #endregion
    }
}