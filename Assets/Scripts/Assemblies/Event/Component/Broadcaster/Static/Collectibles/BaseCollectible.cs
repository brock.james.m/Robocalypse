using System;


namespace Robo.Event.Component
{
    public class BaseCollectible : TriggerEventBroadcasterContainer
    {
        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            EventBroadcaster.OnTriggerSelf += OnCollected;
        }

        #endregion

        #region TriggerEventBroadcaster

        protected virtual void OnCollected()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}