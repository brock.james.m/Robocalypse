using System.Collections.Generic;
using UnityEngine;

using Robo.Constants;
using Robo.Event.Interface;


namespace Robo.Event.Component
{
    public class HealthPack : BaseCollectible
    {
        public int Healing = 100;

        protected float floatSpeed = 12.0f;
        protected float floatHeight = 0.0005f;

        protected List<string> ignoredTags =
            new List<string>()
            {
                ObjTag.PassableTerrain,
                ObjTag.FriendlyProjectile
            };


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            this.TriggerEvent =
                new MessageEventBuilder().Builder()
                    .WithIgnoredTags(this.ignoredTags)
                    .WithEffect(new HealEffect(this.Healing))
                .Build();
        }

        protected override void Update()
        {
            base.Update();

            this.gameObject.transform.position +=
                new Vector3(
                    0.0f,
                    0.0f,
                    Mathf.Sin(floatSpeed * Time.time) * floatHeight
                );
        }

        #endregion


        /* --- Subclass --- */
        protected override void OnCollected()
        {
            Destroy(this.gameObject);
        }
    }
}