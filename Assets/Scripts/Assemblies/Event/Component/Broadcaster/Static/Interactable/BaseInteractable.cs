using System;


namespace Robo.Event.Component
{
    public class BaseInteractable : TriggerEventBroadcasterContainer
    {
        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            EventBroadcaster.OnTriggerSelf += OnEntered;
        }

        #endregion

        #region TriggerEventBroadcaster 

        protected virtual void OnEntered()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}