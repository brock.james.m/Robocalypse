using System.Collections.Generic;
using UnityEngine;

using Robo.Event.Interface;
using Robo.Util.UI;
using Robo.Constants;


namespace Robo.Event.Component
{
    public class ToolBench : BaseInteractable
    {
        protected List<string> ignoredTags =
            new List<string>()
            {
                        ObjTag.FriendlyProjectile,
                                    ObjTag.HostileProjectile,
                                    ObjTag.PassableTerrain
            };

        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            this.TriggerEvent =
                new MessageEventBuilder().Builder()
                    .WithIgnoredTags(this.ignoredTags)
                    .WithMessage("hello there, yarg, i be a toolbench")
                .Build();
        }

        protected override void Start()
        {
            base.Start();

            EventBroadcaster.OnTriggerSelf += OnEntered;
        }

        #endregion


        #region Lifecycle EventBroadcaster

        protected override void OnEntered()
        {
            IMessageEvent messageEvent = (this.TriggerEvent as IMessageEvent);

            FloatingText.Show(
                text: $"{messageEvent.Message}",
                spawnPosition: this.transform.Find("text_spawn").transform.position,
                color: Color.white,
                fontSize: 64
            );
        }

        #endregion
    }
}