using UnityEngine;

using Robo.Base;
using Robo.Event.Interface;


namespace Robo.Event
{
    /// <summary>
    /// Message object that describes how to affect another object
    /// <para>
    /// i.e. Push rigidbody, Freeze rigidbody, Reduce Health, Inflict Weak, ...
    /// </para>
    /// </summary>
    public class BaseEffect : BaseObject, IEffect
    {
        /// <summary>
        /// Time the event was processed by the EventDispatcher
        /// <para>
        /// Used for performance metrics
        /// </para>
        /// </summary>
        public float EventBroadcastTime { get; set; } = 0.0f;

        public override string ToString()
        {
            return LogTag;
        }
    }


    public static class EffectExtensions
    {
        public static Color GetTextColor(this BaseEffect e)
        {
            switch (e)
            {
                case DamageEffect damageEffect:
                    return Color.red;

                case HealEffect healEffect:
                    return Color.green;

                case MoveEffect moveEffect:
                    return Color.yellow;

                default:
                    return Color.white;
            }
        }
    }
}