namespace Robo.Event.Interface
{
    public interface IEffect
    {
        float EventBroadcastTime { get; set; }
    }
}