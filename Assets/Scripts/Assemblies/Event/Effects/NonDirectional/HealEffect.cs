using Robo.Event.Interface;


namespace Robo.Event
{
    public class HealEffect : BaseEffect, IHealEffect
    {
        public int Healing { get; }
        public HealEffect(int healing) { this.Healing = healing; }
    }
}