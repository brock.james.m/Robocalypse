using Robo.Event.Interface;


namespace Robo.Event
{
    public class DamageEffect : BaseEffect, IDamageEffect
    {
        public int Damage { get; }
        public DamageEffect(int damage) { this.Damage = damage; }
    }
}