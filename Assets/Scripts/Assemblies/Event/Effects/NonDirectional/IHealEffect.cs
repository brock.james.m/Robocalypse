namespace Robo.Event.Interface
{
    public interface IHealEffect : IEffect
    {
        int Healing { get; }
    }
}