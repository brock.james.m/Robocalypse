namespace Robo.Event.Interface
{
    public interface IDamageEffect : IEffect
    {
        int Damage { get; }
    }
}