namespace Robo.Event.Interface
{
    public interface IEffectProcessor
    {
        DispatchStatus ProcessEffect(IEffect effect);
    }
}