using System.Collections.Generic;


namespace Robo.Event.Interface
{
    public interface IEffectDispatcher
    {
        DispatchStatus[] DispatchEffects(List<IEffect> effects);
        DispatchStatus[] DispatchEffects(IEvent e);

        EffectFilter EffectFilter { get; }
        bool Allows(EffectFilter e);
        void Allow(EffectFilter e);
        void Ignore(EffectFilter e);
    }
}

