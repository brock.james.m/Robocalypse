using System;
using System.Collections.Generic;
using System.Linq;

using Robo.Base;
using Robo.Event.Interface;
using Robo.Log;
using Robo.Util;


namespace Robo.Event
{
    public class EffectDispatcher : BaseObject, IEffectDispatcher
    {
        protected Dictionary<Type, IEffectProcessor> Processors { get; private set; }

        public EffectDispatcher(
            EffectFilter effectFilter = EffectFilter.All,
            IEffectProcessor healthProcessor = null,
            IEffectProcessor moverProcessor = null)
        {
            this.effectFilter = effectFilter;

            this.Processors = new Dictionary<Type, IEffectProcessor>()
            {
                { typeof(HealEffect), healthProcessor },
                { typeof(DamageEffect), healthProcessor },
                { typeof(MoveEffect), moverProcessor }
            };
        }


        /* --- IEffectDispatcher --- */
        protected EffectFilter effectFilter;
        public EffectFilter EffectFilter => effectFilter;
        public void Allow(EffectFilter e)
        {
            effectFilter.Add(e);
        }
        public void Ignore(EffectFilter e)
        {
            effectFilter.Remove(e);
        }
        public bool Allows(EffectFilter e)
        {
            return effectFilter.Allows(e);
        }

        public DispatchStatus[] DispatchEffects(List<IEffect> effects)
        {
            return effects.Select(e => DispatchEffect(e)).ToArray();
        }

        public DispatchStatus[] DispatchEffects(IEvent e)
        {
            return DispatchEffects(e.Effects);
        }

        /* --- Helpers --- */
        private DispatchStatus DispatchEffect(IEffect effect)
        {
            Type effectType = effect.GetType();

            IEffectProcessor processor = null;
            DispatchStatus status = DispatchStatus.Failed;

            if (!Processors.TryGetValue(effectType, out processor) || processor is null)
            {
                this.LogW($"\nFailed to get IEffectProcessor for {effectType}");
                return status;
            }

            status =
                this.effectFilter.Allows(EffectFilterExtensions.FromEffect(effect))
                    ? processor.ProcessEffect(effect)
                    : DispatchStatus.Filtered;

            this.LogD($"ProcessEffect {status}" + $"\n{effect}".Indent(2));
            return status;
        }
    }
}

