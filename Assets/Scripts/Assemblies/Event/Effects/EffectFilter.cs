using System;


namespace Robo.Event.Interface
{
    [Flags]
    public enum EffectFilter
    {
        None = 0x0000,
        Damage = 0x0001,
        Heal = 0x0002,
        Move = 0x0004,

        /// <summary>
        /// Max Value. Update when adding an EventFilter flag.
        /// </summary>
        All = ~(-1 << Move)
    };


    public static class EffectFilterExtensions
    {
        public static void Add(ref this EffectFilter flags, EffectFilter addFlag)
        {
            flags |= addFlag;
        }

        public static void Remove(ref this EffectFilter flags, EffectFilter removeFlag)
        {
            flags &= ~removeFlag;
        }

        public static bool Allows(ref this EffectFilter flags, EffectFilter checkFlags)
        {
            return flags.HasFlag(checkFlags);
        }

        public static EffectFilter FromEffect(IEffect effect)
        {
            switch (effect)
            {
                case DamageEffect damageEffect:
                    return EffectFilter.Damage;

                case HealEffect h:
                    return EffectFilter.Heal;

                case MoveEffect moveEffect:
                    return EffectFilter.Move;

                default:
                    throw new NotImplementedException();
            }
        }
    }
}