using Robo.Event.Interface;
using Robo.Util;


namespace Robo.Event
{
    public class MoveEffect : DirectionalEffect, IMoveEffect
    {
        public float Duration { get; }
        public IInterpolator Speed { get; set; }
        public IInterpolator RotateDegrees { get; set; }

        public MoveEffect(
            float duration,
            IInterpolator speed,
            IInterpolator rotateDegrees = null)
        {
            this.Duration = duration;
            this.Speed = speed;
            this.RotateDegrees = rotateDegrees ?? KnockbackSpeed(0.0f);
        }

        public static IInterpolator KnockbackSpeed(float knockbackSpeed)
        {
            return FloatInterpolator.CreateConstant(knockbackSpeed);
        }

        public static IInterpolator DashSpeed(float p0, float p1, float p2)
        {
            return FloatInterpolator.CreateQuadratic(p0, p1, p2);
        }

        public override string ToString()
        {
            string output = $"{this.GetType().Name}";

            output += $"\nDuration:               {Duration}".Indent(4);
            output += $"\nInitialFacingDirection: {FacingDirection}".Indent(4);
            output += $"\n{Speed}".Indent(4);
            output += $"\n{RotateDegrees}".Indent(4);

            return output;
        }
    }
}