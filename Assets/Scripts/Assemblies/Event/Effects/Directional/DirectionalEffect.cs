using UnityEngine;

using Robo.Event.Interface;


namespace Robo.Event
{
    public class DirectionalEffect : BaseEffect, IDirectionalEffect
    {
        public Vector2 FacingDirection { get; set; }
    }
}