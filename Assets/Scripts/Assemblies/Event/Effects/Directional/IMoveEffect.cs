using Robo.Util;


namespace Robo.Event.Interface
{
    public interface IMoveEffect : IDirectionalEffect
    {
        float Duration { get; }
        IInterpolator Speed { get; set; }
        IInterpolator RotateDegrees { get; set; }
    }
}