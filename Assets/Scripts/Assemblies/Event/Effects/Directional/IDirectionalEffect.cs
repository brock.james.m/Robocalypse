using Robo.Util.Vector;


namespace Robo.Event.Interface
{
    public interface IDirectionalEffect : IEffect, IFacingDirection
    {
    }
}