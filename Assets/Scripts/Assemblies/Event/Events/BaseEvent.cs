using System.Collections.Generic;
using UnityEngine;

using Robo.Base;
using Robo.Event.Interface;
using Robo.Util;


namespace Robo.Event
{
    /// <summary>
    /// Basic message structure that defines an event. Should be instantiated through an EventBuilder.
    /// </summary>
    internal class BaseEvent : BaseObject, IEvent
    {
        /// <summary>
        /// The 'event definition' in the eyees of the EventDispatcher
        /// </summary>
        public EventFilter EventFlags { get => eventFlags; set => eventFlags = value; }
        [SerializeField] protected EventFilter eventFlags;

        /// <summary>
        /// List of things that should happen to the event receiver
        /// </summary>
        public List<IEffect> Effects { get; set; }

        /// <summary>
        /// Time the event was processed by the EventDispatcher
        /// <para>
        /// Used for performance metrics
        /// </para>
        /// </summary>
        public float BroadcastTime
        {
            get => broadcastTime;
            set
            {
                broadcastTime = value;
                foreach (IEffect effect in Effects)
                {
                    effect.EventBroadcastTime = BroadcastTime;
                }
            }
        }
        protected float broadcastTime = 0.0f;

        /// <summary>
        /// Print all Event and Effect information
        /// </summary>
        public override string ToString()
        {
            string output = $"{this.GetType().Name} (time={BroadcastTime})";

            output += $"\nEventFlags: {EventFlags}".Indent(2);

            if (Effects.Count > 0)
            {
                output += $"\nEffects:".Indent(2);
                foreach (BaseEffect effect in Effects)
                {
                    output += $"\n{effect}".Indent(4);
                }
            }

            return output;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseEvent()
        {
            this.Effects = new List<IEffect>();
        }
    }
}