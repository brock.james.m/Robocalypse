using Robo.Event.Interface;


namespace Robo.Event
{
    internal class ActionEvent : DirectionalEvent, IActionEvent
    {
        public ActionEvent()
        {
            this.EventFlags = this.EventFlags.Allow(EventFilter.Action);
        }
    }
}