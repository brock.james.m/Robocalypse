using Robo.Event.Interface;


namespace Robo.Event
{
    internal class RangedAttackEvent : TriggerEvent
    {
        public RangedAttackEvent()
        {
            this.EventFlags = this.EventFlags.Allow(EventFilter.Ranged | EventFilter.Attack);
        }
    }
}