using Robo.Event.Interface;


namespace Robo.Event
{
    internal class MeleeAttackEvent : TriggerEvent
    {
        public MeleeAttackEvent()
        {
            this.EventFlags = this.EventFlags.Allow(EventFilter.Melee | EventFilter.Attack);
        }
    }
}