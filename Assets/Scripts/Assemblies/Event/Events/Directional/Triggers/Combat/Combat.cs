using Robo.Event.Interface;


namespace Robo.Event.Interface
{
    public interface IMeleeAttackEvent : ITriggerEvent { };
    public interface IRangedAttackEvent : ITriggerEvent { };
    public interface IMeleeSpellEvent : ITriggerEvent { };
    public interface IMRangedSpellEvent : ITriggerEvent { };
}