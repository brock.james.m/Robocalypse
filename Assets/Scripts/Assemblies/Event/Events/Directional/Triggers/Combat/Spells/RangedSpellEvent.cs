using Robo.Event.Interface;


namespace Robo.Event
{
    internal class RangedSpellEvent : TriggerEvent
    {
        public RangedSpellEvent()
        {
            this.EventFlags = this.EventFlags.Allow(EventFilter.Ranged | EventFilter.Spell);
        }
    }
}