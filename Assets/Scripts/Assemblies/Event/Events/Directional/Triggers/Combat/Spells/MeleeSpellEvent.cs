using Robo.Event.Interface;


namespace Robo.Event
{
    internal class MeleeSpellEvent : TriggerEvent
    {
        public MeleeSpellEvent()
        {
            this.EventFlags = this.EventFlags.Allow(EventFilter.Melee | EventFilter.Spell);
        }
    }
}