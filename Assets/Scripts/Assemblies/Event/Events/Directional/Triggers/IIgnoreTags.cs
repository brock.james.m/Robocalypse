using System.Collections.Generic;


namespace Robo.Event.Interface
{
    public interface IIgnoreTags
    {
        List<string> IgnoredTags { get; set; }
    }
}