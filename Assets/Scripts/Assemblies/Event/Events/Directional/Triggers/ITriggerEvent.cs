namespace Robo.Event.Interface
{
    public interface ITriggerEvent : IDirectionalEvent, IIgnoreTags
    {
    }
}