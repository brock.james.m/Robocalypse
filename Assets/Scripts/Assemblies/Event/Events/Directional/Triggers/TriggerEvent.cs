using System.Collections.Generic;

using Robo.Event.Interface;


namespace Robo.Event
{
    internal class TriggerEvent : DirectionalEvent, ITriggerEvent
    {
        protected List<string> ignoredTags = new List<string>();
        public List<string> IgnoredTags
        {
            get => ignoredTags;
            set => ignoredTags = value;
        }
    }
}