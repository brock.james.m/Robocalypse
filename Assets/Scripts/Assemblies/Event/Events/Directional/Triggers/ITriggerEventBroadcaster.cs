using System;


namespace Robo.Event.Interface.Trigger
{
    public interface ITriggerEventBroadcaster : IEventBroadcaster
    {
        event Action OnTriggerSelf;
    }
}