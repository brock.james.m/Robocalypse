namespace Robo.Event.Interface
{
    public interface IMessageEvent : ITriggerEvent
    {
        string Message { get; set; }
    }
}