namespace Robo.Event.Interface
{
    internal class MessageEvent : TriggerEvent, IMessageEvent
    {
        public string Message { get; set; }
    }
}