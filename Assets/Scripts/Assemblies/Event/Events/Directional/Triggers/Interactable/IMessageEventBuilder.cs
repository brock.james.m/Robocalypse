namespace Robo.Event.Interface
{
    public interface IMessageEventBuilder<TBuilder, TEvent>
        : ITriggerEventBuilder<TBuilder, TEvent>
    {
        TBuilder WithMessage(string message);
    }
}