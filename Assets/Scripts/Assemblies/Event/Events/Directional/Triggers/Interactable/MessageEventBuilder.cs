namespace Robo.Event.Interface
{
    /// <summary>
    /// Extend to build any event that implements IMessageEvent
    /// </summary>
    /// <typeparam name="TBuilder"></typeparam>
    /// <typeparam name="TEvent"></typeparam>
    public class BaseMessageEventBuilder<TBuilder, TEvent>
        : BaseTriggerEventBuilder<TBuilder, TEvent>
        , IMessageEventBuilder<TBuilder, TEvent>
        where TBuilder : class, IMessageEventBuilder<TBuilder, TEvent>, new()
        where TEvent : IMessageEvent
    {
        public TBuilder WithMessage(string message)
        {
            this.e.Message = message;
            return this as TBuilder;
        }
    }

    /// <summary>
    /// For instantiating MessageEvents
    /// </summary>
    public class MessageEventBuilder
        : BaseMessageEventBuilder<MessageEventBuilder, IMessageEvent>
    {
    }
}