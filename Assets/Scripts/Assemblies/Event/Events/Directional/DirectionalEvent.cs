using UnityEngine;

using Robo.Event.Interface;
using Robo.Util.Vector;


namespace Robo.Event
{
    internal class DirectionalEvent : BaseEvent, IDirectionalEvent
    {
        protected Vector2 facingDirection = Vector2.zero;
        public Vector2 FacingDirection
        {
            get => facingDirection;
            set
            {
                facingDirection = value.normalized;

                foreach (BaseEffect effect in this.Effects)
                {
                    IFacingDirection hit = effect as IFacingDirection;
                    if (hit != null)
                    {
                        hit.FacingDirection = facingDirection;
                    }
                }
            }
        }
    }
}