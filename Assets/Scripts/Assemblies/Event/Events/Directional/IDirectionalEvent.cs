using Robo.Util.Vector;


namespace Robo.Event.Interface
{
    public interface IDirectionalEvent : IEvent, IFacingDirection
    {
    }
}