using System.Collections.Generic;


namespace Robo.Event.Interface
{
    public interface IEvent
    {
        EventFilter EventFlags { get; set; }
        List<IEffect> Effects { get; }
        float BroadcastTime { get; set; }
    }
}