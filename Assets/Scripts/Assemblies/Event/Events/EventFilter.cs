using System;


namespace Robo.Event.Interface
{
    [Flags]
    public enum EventFilter
    {
        None = 0x0000,
        Melee = 0x0001,
        Ranged = 0x0002,
        Attack = 0x0004,
        Spell = 0x0008,
        Action = 0x0010,

        /// <summary>
        /// Max Value. Update when adding an EventFilter flag.
        /// </summary>
        All = ~(-1 << Action)
    };


    public static class EventFilterExtensions
    {
        public static EventFilter Allow(this EventFilter flags, EventFilter addFlag)
        {
            // TODO: no ref ok?
            return flags |= addFlag;
        }

        public static EventFilter Ignore(this EventFilter flags, EventFilter removeFlag)
        {
            return flags &= ~removeFlag;
        }

        public static bool Allows(this EventFilter flags, EventFilter checkFlags)
        {
            return flags.HasFlag(checkFlags);
        }

        public static bool Allows(this EventFilter flags, IEvent e)
        {
            return flags.HasFlag(e.EventFlags);
        }

        public static EffectFilter FromEffect(IEffect effect)
        {
            switch (effect)
            {
                case IDamageEffect damageEffect:
                    return EffectFilter.Damage;

                case IHealEffect h:
                    return EffectFilter.Heal;

                case IMoveEffect moveEffect:
                    return EffectFilter.Move;

                default:
                    throw new NotImplementedException();
            }
        }
    }
}