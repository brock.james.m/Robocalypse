using System;
using System.Linq;


namespace Robo.Event.Interface
{
    /// <summary>
    /// Return value for IEventDispatcher
    /// </summary>
    public class EventDispatcherStatus
    {
        protected DispatchStatus _event;
        public DispatchStatus Event { private get => _event; set => _event = value; }
        public bool EventSucceeded => (Event == DispatchStatus.Success);
        public bool EventFiltered => (Event == DispatchStatus.Filtered);

        public int[] Effects = new int[Enum.GetNames(typeof(DispatchStatus)).Length];
        public int NumEffects => Effects.Sum();
        public int EffectsSucceeded => Effects[(int)DispatchStatus.Success];
        public int EffectsFiltered => Effects[(int)DispatchStatus.Filtered];
        public int EffectsFailed => Effects[(int)DispatchStatus.Failed];


        public void Clear()
        {
            Event = DispatchStatus.Failed;
            Array.Clear(Effects, 0, Effects.Length);
        }

        public override string ToString()
        {
            string output = "EventProcessed\n";

            output += $"Event: {Event}";

            output += "Effect\n";
            int i = 0;
            foreach (string status in Enum.GetNames(typeof(DispatchStatus)))
            {
                int numOccured = Effects[i++];

                if (numOccured > 0)
                {
                    output += $"  {numOccured} {status}\n";
                }
            }

            return output;
        }
    }
}