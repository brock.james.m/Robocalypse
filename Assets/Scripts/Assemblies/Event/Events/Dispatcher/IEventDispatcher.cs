namespace Robo.Event.Interface
{
    public interface IEventDispatcher
    {
        EventDispatcherStatus DispatchEvent(IEvent e);

        EventFilter EventFilter { get; }
        bool Allows(EventFilter e);
        void Allow(EventFilter e);
        void Ignore(EventFilter e);

        IEffectDispatcher EffectDispatcher { get; }
    }
}