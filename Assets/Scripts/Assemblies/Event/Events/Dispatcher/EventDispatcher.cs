using UnityEngine;

using Robo.Base;
using Robo.Event.Interface;
using Robo.Log;
using Robo.Util;


namespace Robo.Event
{
    public class EventDispatcher : BaseObject, IEventDispatcher
    {
        protected IEffectDispatcher effectDispatcher;
        public IEffectDispatcher EffectDispatcher => effectDispatcher;

        public EventDispatcher(
            IEffectDispatcher effectDispatcher,
            EventFilter eventFilter = EventFilter.None)
        {
            this.effectDispatcher = effectDispatcher;
            this.eventFilter = eventFilter;
        }


        /* --- IEventDispatcher --- */
        private EventFilter eventFilter;
        public EventFilter EventFilter => eventFilter;
        public void Allow(EventFilter e)
        {
            eventFilter = eventFilter.Allow(e);
        }
        public void Ignore(EventFilter e)
        {
            eventFilter = eventFilter.Ignore(e);
        }
        public bool Allows(EventFilter e)
        {
            return eventFilter.Allows(e);
        }


        // TODO: Is Thread safety a concern with Unity and these events?
        // This should be called synchronously by each broadcaster in OnTrigger() etc
        private EventDispatcherStatus EventDispatcherStatus = new EventDispatcherStatus();
        public EventDispatcherStatus DispatchEvent(IEvent e)
        {
            e.BroadcastTime = Time.time;

            this.LogD(
                $"Allowed: {this.EventFilter}"
                + $"\n{e}".Indent(2)
                );

            EventDispatcherStatus.Clear();

            if (EventFilter.Allows(e))
            {
                EventDispatcherStatus.Event = DispatchStatus.Success;

                foreach (DispatchStatus status in EffectDispatcher.DispatchEffects(e.Effects))
                {
                    EventDispatcherStatus.Effects[(int)status]++;
                }
            }
            else
            {
                this.LogD($"{e.GetType().Name} filtered");
                EventDispatcherStatus.Event = DispatchStatus.Filtered;
            }

            return EventDispatcherStatus;
        }
    }
}