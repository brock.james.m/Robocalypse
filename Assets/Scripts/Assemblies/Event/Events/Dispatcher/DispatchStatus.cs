namespace Robo.Event.Interface
{
    /// <summary>
    ///  Return value for a dispatcher
    /// </summary>
    public enum DispatchStatus
    {
        Success,
        Filtered,
        Failed
    }
}