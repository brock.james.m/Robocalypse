using System;

using UnityEngine;

using Robo.Event.Interface;
using Robo.Event.Interface.Trigger;
using Robo.Log;


namespace Robo.Event
{
    public class TriggerEventBroadcaster : EventBroadcaster, ITriggerEventBroadcaster
    {
        public event Action OnTriggerSelf;

        public override bool BroadcastTo(
            GameObject other,
            IEventDispatcher dispatcher,
            IEvent e)
        {
            TriggerEvent triggerEvent = e as TriggerEvent;

            if (triggerEvent.IgnoredTags.Contains(other.tag))
            {
                this.LogD($"Ignored '{other.tag}' {other.name}");
                return false;
            }

            this.LogD($"Hit '{other.tag}' {other.name}");

            if (base.BroadcastTo(other, dispatcher, triggerEvent))
            {
                OnTriggerSelf?.Invoke();
                return true;
            }

            return false;
        }
    }
}