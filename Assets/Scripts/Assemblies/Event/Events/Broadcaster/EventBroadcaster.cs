using System;
using UnityEngine;

using Robo.Base;
using Robo.Event.Interface;


namespace Robo.Event
{
    public class EventBroadcaster : BaseObject, IEventBroadcaster
    {
        /// <summary>
        /// Invoked when BroadcastTo() causes 'other' to invoke a Processor
        /// </summary>
        public event Action OnEventSucceeded;

        /// <summary>
        /// Send an Event to another EventBasedComponent
        /// </summary>
        /// <param name="dispatcher"></param>
        /// <param name="e"></param>
        /// <returns>True if 'other' invoked a Processor</returns>
        public virtual bool BroadcastTo(
            GameObject other,
            IEventDispatcher dispatcher,
            IEvent e)
        {
            if (dispatcher?.DispatchEvent(e).EventSucceeded ?? false)
            {
                OnEventSucceeded?.Invoke();
                return true;
            }

            return false;
        }
    }
}