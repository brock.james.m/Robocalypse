using System;
using UnityEngine;


namespace Robo.Event.Interface
{
    public interface IEventBroadcaster
    {
        event Action OnEventSucceeded;
        bool BroadcastTo(
            GameObject other,
            IEventDispatcher dispatcher,
            IEvent e);
    }
}