using UnityEngine;

using Robo.Util.Vector;


namespace Robo.Event.Interface
{
    /// <summary>
    /// Extend to build any event that implements IDirectionalEvent
    /// </summary>
    /// <typeparam name="TBuilder"></typeparam>
    /// <typeparam name="TEvent"></typeparam>
    public class BaseDirectionalEventBuilder<TBuilder, TEvent>
        : EventBuilder<TBuilder, TEvent>
        , IDirectionalEventBuilder<TBuilder, TEvent>
        where TBuilder : class, IDirectionalEventBuilder<TBuilder, TEvent>, new()
        where TEvent : IDirectionalEvent
    {
        public TBuilder Facing(Vector2 facing)
        {
            (this.e as IFacingDirection).FacingDirection = facing;
            return this as TBuilder;
        }
    }

    /// <summary>
    /// For instantiating DirectionalEvents
    /// </summary>
    public class DirectionalEventBuilder
        : BaseDirectionalEventBuilder<DirectionalEventBuilder, IDirectionalEvent>
    {
    }
}