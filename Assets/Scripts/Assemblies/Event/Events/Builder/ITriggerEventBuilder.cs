using System.Collections.Generic;


namespace Robo.Event.Interface
{
    public interface ITriggerEventBuilder<TBuilder, TEvent>
        : IDirectionalEventBuilder<TBuilder, TEvent>
    {
        TBuilder WithIgnoredTags(List<string> ignoredTags);
    }
}
