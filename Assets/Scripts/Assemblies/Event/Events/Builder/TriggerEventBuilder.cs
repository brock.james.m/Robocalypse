using System.Collections.Generic;


namespace Robo.Event.Interface
{
    /// <summary>
    /// Extend to build any event that implements ITriggerEvent
    /// </summary>
    /// <typeparam name="TBuilder"></typeparam>
    /// <typeparam name="TEvent"></typeparam>
    public class BaseTriggerEventBuilder<TBuilder, TEvent>
        : BaseDirectionalEventBuilder<TBuilder, TEvent>
        , ITriggerEventBuilder<TBuilder, TEvent>
        where TBuilder : class, ITriggerEventBuilder<TBuilder, TEvent>, new()
        where TEvent : ITriggerEvent
    {
        public TBuilder WithIgnoredTags(List<string> ignoredTags)
        {
            this.e.IgnoredTags = ignoredTags;
            return this as TBuilder;
        }
    }

    /// <summary>
    /// For instantiating TriggerEvents
    /// </summary>
    public class TriggerEventBuilder
        : BaseTriggerEventBuilder<TriggerEventBuilder, ITriggerEvent>
    {
    }
}
