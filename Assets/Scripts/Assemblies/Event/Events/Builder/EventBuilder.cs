using System.Collections.Generic;


namespace Robo.Event.Interface
{
    public class EventBuilder<TBuilder, TEvent>
        : IEventBuilder<TBuilder, TEvent>
        where TBuilder : class, IEventBuilder<TBuilder, TEvent>, new()
        where TEvent : IEvent
    {
        protected TEvent e;

        public TBuilder Builder()
        {
            e = (TEvent)Util.GetBaseImplementation<TEvent>();
            return this as TBuilder;
        }

        public TBuilder WithOverrideEventFlags(EventFilter eventFlags)
        {
            this.e.EventFlags = eventFlags;
            return this as TBuilder;
        }

        public TBuilder WithEffect(IEffect effect)
        {
            this.e.Effects.Add(effect);
            return this as TBuilder;
        }

        public TBuilder WithEffects(List<IEffect> effects)
        {
            this.e.Effects.AddRange(effects);
            return this as TBuilder;
        }

        public virtual TEvent Build()
        {
            return e;
        }
    }
}