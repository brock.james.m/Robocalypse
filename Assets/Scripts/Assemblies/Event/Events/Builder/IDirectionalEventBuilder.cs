using UnityEngine;


namespace Robo.Event.Interface
{
    public interface IDirectionalEventBuilder<TBuilder, TEvent>
        : IEventBuilder<TBuilder, TEvent>
    {
        TBuilder Facing(Vector2 facing);
    }
}