using System.Collections.Generic;


namespace Robo.Event.Interface
{
    /// <summary>
    /// Extend to build any event that implements IActionEvent
    /// </summary>
    /// <typeparam name="TBuilder"></typeparam>
    /// <typeparam name="TEvent"></typeparam>
    public class BaseActionEventBuilder<TBuilder, TEvent>
        : BaseDirectionalEventBuilder<TBuilder, TEvent>
        , IActionEventBuilder<TBuilder, TEvent>
        where TBuilder : class, IActionEventBuilder<TBuilder, TEvent>, new()
        where TEvent : IActionEvent
    {
    }

    /// <summary>
    /// For instantiating ActionEvents
    /// </summary>
    public class ActionEventBuilder
        : BaseActionEventBuilder<ActionEventBuilder, IActionEvent>
    {
    }
}
