namespace Robo.Event.Interface
{
    public interface IActionEventBuilder<TBuilder, TEvent>
        : IDirectionalEventBuilder<TBuilder, TEvent>
    {
    }
}
