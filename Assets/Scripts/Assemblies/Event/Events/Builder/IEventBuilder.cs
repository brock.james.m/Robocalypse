using System.Collections.Generic;


namespace Robo.Event.Interface
{
    public interface IEventBuilder<TBuilder, TEvent>
    {
        TBuilder Builder();
        TBuilder WithOverrideEventFlags(EventFilter eventFlags);
        TBuilder WithEffect(IEffect effect);
        TBuilder WithEffects(List<IEffect> effects);
        TEvent Build();
    }
}
