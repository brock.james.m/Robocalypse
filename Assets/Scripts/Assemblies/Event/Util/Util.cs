using System;
using System.Collections.Generic;

using Robo.Event.Interface;
using Robo.Log;


namespace Robo.Event
{
    public class Util
    {
        public static string StaticLogTag = "Robo.Event.Util";

        private static Type tDirectionalEvent = typeof(IDirectionalEvent);

        private static Dictionary<Type, Type> baseImpls =
            new Dictionary<Type, Type>()
            {
                {typeof(IActionEvent), typeof(ActionEvent)}
            };

        public static IEvent GetBaseImplementation<IEventType>()
        {
            Type t = typeof(IEventType);

            if (tDirectionalEvent.IsAssignableFrom(t))
            {
                if (t == typeof(IActionEvent)) return new ActionEvent();
                else if (t == typeof(ITriggerEvent)) return new TriggerEvent();
                else if (t == typeof(IMessageEvent)) return new MessageEvent();
            }

            Logger.Instance.I(StaticLogTag, $"GetBaseImplementation({t.Name})");
            throw new NotImplementedException();
        }
    }
}