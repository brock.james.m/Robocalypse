using System;


namespace Robo.State
{
    public interface IStateMachine
    {
        /// <summary>
        /// Progresses the state machine.
        /// <para>
        /// To be called regularly (i.e. on MonoBehaviour Update)
        /// </para>
        /// </summary>
        void Progress();

        /// <summary>
        /// Sets the current state. (?) Only use for initializing the state machine.
        /// </summary>
        /// <param name="newState"></param>
        void SetState(IState newState);

        /// <summary>
        /// Add a condition (predicate) on which we transition from state 'from' to state 'to'
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="predicate"></param>
        void AddTransition(IState from, IState to, Func<bool> predicate);

        /// <summary>
        /// Add a condition (predicate) on which we transition from any state to state 'to'
        /// </summary>
        /// <param name="state"></param>
        /// <param name="predicate"></param>
        void AddAnyTransition(IState state, Func<bool> predicate);
    }
}