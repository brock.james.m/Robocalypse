using System;
using System.Collections.Generic;


namespace Robo.State
{
    public class StateMachine : IStateMachine
    {
        private IState curState;
        private Dictionary<Type, List<Transition>> transitions = new Dictionary<Type, List<Transition>>();
        private List<Transition> curStateTransitions = new List<Transition>();
        private List<Transition> anyTransitions = new List<Transition>();
        private static List<Transition> EmptyTransitions = new List<Transition>(0);


        #region IStateMachine

        public void Progress()
        {
            var transition = GetTransition();
            if (transition != null)
            {
                SetState(transition.To);
            }

            curState?.Progress();
        }

        public void SetState(IState newState)
        {
            if (newState == curState)
            {
                return;
            }

            curState?.OnExit();
            curState = newState;

            if (!transitions.TryGetValue(curState.GetType(), out curStateTransitions))
            {
                curStateTransitions = EmptyTransitions;
            }

            curState.OnEnter();
        }

        public void AddTransition(IState from, IState to, Func<bool> predicate)
        {
            if (!transitions.TryGetValue(from.GetType(), out var stateTransitions))
            {
                stateTransitions = new List<Transition>();
                transitions[from.GetType()] = stateTransitions;
            }

            stateTransitions.Add(new Transition(to, predicate));
        }

        public void AddAnyTransition(IState state, Func<bool> predicate)
        {
            anyTransitions.Add(new Transition(state, predicate));
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        private class Transition
        {
            public Func<bool> Condition { get; }
            public IState To { get; }

            public Transition(IState to, Func<bool> condition)
            {
                this.To = to;
                this.Condition = condition;
            }
        }

        /// <summary>
        /// Checks transition lists to see if any conditions are satisfied.
        /// <para>
        /// Prioritizes 'any' transitions before current state transitions
        /// </para>
        /// </summary>
        private Transition GetTransition()
        {
            foreach (var transition in anyTransitions)
            {
                if (transition.Condition())
                    return transition;
            }

            foreach (var transition in curStateTransitions)
            {
                if (transition.Condition())
                    return transition;
            }

            return null;
        }
    }
}