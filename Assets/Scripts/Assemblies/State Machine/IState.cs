namespace Robo.State
{
    public interface IState
    {
        void Progress();
        void OnEnter();
        void OnExit();
    }
}