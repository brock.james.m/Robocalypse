﻿using UnityEngine;

using Robo.Base;
using Robo.Log;


namespace Robo.LFX
{
    public class LightEffect : BaseComponent
    {
        public Preset preset = Preset.None;
        public Effect effect;

        [Range(0.1f, 4.0f)]
        public float strobePeriodSeconds;
        [Range(0.0f, 2.0f)]
        public float intensityMin;
        [Range(0.0f, 2.0f)]
        public float intensityMax;
        [Range(0.1f, 0.6f)]
        public float minFlickerTime;
        [Range(0.6f, 1.0f)]
        public float maxFlickerTime;

        // Use the same name if you wish 2 lights to be coordinated in their effect
        public string coordinatorName;
        LightEffectController lfxCoordinator;

        private UnityEngine.Experimental.Rendering.Universal.Light2D light2d;

        // Delegate for light function
        private delegate void FnEffect();
        FnEffect fnEffect = null;

        public enum Preset
        {
            None = 0,
            Idle,
            SlowStrobe,
            FastStrobe,
            FlickerLight,
            FlickerBrokenLight,
            FlickerLava
        }
        private readonly float SLOWSTROBE_PERIOD_SECONDS = 1.5f;
        private readonly float SLOWSTROBE_INTENSITY_MIN = 0.9f;
        private readonly float SLOWSTROBE_INTENSITY_MAX = 1.0f;

        private readonly float FASTSTROBE_PERIOD_SECONDS = 0.4f;
        private readonly float FASTSTROBE_INTENSITY_MIN = 0.8f;
        private readonly float FASTSTROBE_INTENSITY_MAX = 1.2f;

        private readonly float FLICKER_LIGHT_INTENSITY_MIN = 0.7f;
        private readonly float FLICKER_LIGHT_INTENSITY_MAX = 1.0f;
        private readonly float FLICKER_LIGHT_MIN_FLICKER_TIME = 0.1f;
        private readonly float FLICKER_LIGHT_MAX_FLICKER_TIME = 0.5f;

        private readonly float FLICKER_BROKEN_LIGHT_INTENSITY_MIN = 0.0f;
        private readonly float FLICKER_BROKEN_LIGHT_INTENSITY_MAX = 1.0f;
        private readonly float FLICKER_BROKEN_LIGHT_MIN_FLICKER_TIME = 0.1f;
        private readonly float FLICKER_BROKEN_LIGHT_MAX_FLICKER_TIME = 0.5f;

        private readonly float FLICKER_LAVA_INTENSITY_MIN = 0.7f;
        private readonly float FLICKER_LAVA_INTENSITY_MAX = 1.0f;
        private readonly float FLICKER_LAVA_MIN_FLICKER_TIME = 0.1f;
        private readonly float FLICKER_LAVA_MAX_FLICKER_TIME = 0.5f;

        public enum Effect
        {
            Idle = 0,
            Strobe,
            Flicker
        }

        protected override void Awake()
        {
            base.Awake();

            LightEffectController.Init();
            lfxCoordinator = LightEffectController.GetCoordinator(coordinatorName);
            light2d = GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>();

            this.LogV(name + " using preset '" + preset.ToString() + "'");
            switch (preset)
            {
                case Preset.None:
                case Preset.Idle:
                    break;
                case Preset.SlowStrobe:
                    effect = Effect.Strobe;
                    strobePeriodSeconds = SLOWSTROBE_PERIOD_SECONDS;
                    intensityMin = SLOWSTROBE_INTENSITY_MIN;
                    intensityMax = SLOWSTROBE_INTENSITY_MAX;
                    break;
                case Preset.FastStrobe:
                    effect = Effect.Strobe;
                    strobePeriodSeconds = FASTSTROBE_PERIOD_SECONDS;
                    intensityMin = FASTSTROBE_INTENSITY_MIN;
                    intensityMax = FASTSTROBE_INTENSITY_MAX;
                    break;
                case Preset.FlickerLight:
                    effect = Effect.Flicker;
                    intensityMin = FLICKER_LIGHT_INTENSITY_MIN;
                    intensityMax = FLICKER_LIGHT_INTENSITY_MAX;
                    minFlickerTime = FLICKER_LIGHT_MIN_FLICKER_TIME;
                    maxFlickerTime = FLICKER_LIGHT_MAX_FLICKER_TIME;
                    break;
                case Preset.FlickerBrokenLight:
                    effect = Effect.Flicker;
                    intensityMin = FLICKER_BROKEN_LIGHT_INTENSITY_MIN;
                    intensityMax = FLICKER_BROKEN_LIGHT_INTENSITY_MAX;
                    minFlickerTime = FLICKER_BROKEN_LIGHT_MIN_FLICKER_TIME;
                    maxFlickerTime = FLICKER_BROKEN_LIGHT_MAX_FLICKER_TIME;
                    break;
                case Preset.FlickerLava:
                    effect = Effect.Flicker;
                    intensityMin = FLICKER_LAVA_INTENSITY_MIN;
                    intensityMax = FLICKER_LAVA_INTENSITY_MAX;
                    minFlickerTime = FLICKER_LAVA_MIN_FLICKER_TIME;
                    maxFlickerTime = FLICKER_LAVA_MAX_FLICKER_TIME;
                    break;
                default:
                    break;
            }

            this.LogV(name + " using effect '" + effect.ToString() + "'");
            switch (effect)
            {
                case Effect.Idle:
                    fnEffect = null;
                    break;
                case Effect.Strobe:
                    fnEffect = Strobe;
                    float period = (2.0f * Mathf.PI) / strobePeriodSeconds;
                    float amplitude = (intensityMax - intensityMin) / 2;
                    lfxCoordinator.InitStrobe(Time.time, period, (intensityMax - intensityMin) / 2, intensityMin + amplitude);
                    break;
                case Effect.Flicker:
                    fnEffect = Flicker;
                    lfxCoordinator.InitFlicker(intensityMin, intensityMax, minFlickerTime, maxFlickerTime);
                    break;
                default:
                    break;
            }
        }

        protected override void Update()
        {
            lfxCoordinator.Progress();
            fnEffect?.Invoke();
        }

        void Strobe()
        {
            light2d.intensity = lfxCoordinator.GetStrobeIntensity();
        }

        void Flicker()
        {
            light2d.intensity = lfxCoordinator.GetFlickerIntensity();
        }
    }
}