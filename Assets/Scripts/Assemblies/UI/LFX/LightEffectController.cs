﻿using System.Collections.Generic;
using UnityEngine;

using Robo.Base;
using Robo.Log;


namespace Robo.LFX
{

    // TODO: Refactor to be a BaseComponent
    public class LightEffectController : BaseObject
    {
        public static string StaticLogTag => "LightEffectController";

        public static Dictionary<string, LightEffectController> DictEffectCoordinators { get; private set; }

        private string coordinatorName;
        private LightEffect.Effect effect;

        // General
        public float intensityMin;
        public float intensityMax;

        // Strobe
        public float timeStrobeStart;
        public float strobePeriod;
        public float strobeAmplitude;
        public float strobeOffset;

        // Flicker
        public float minFlickerTime;
        public float maxFlickerTime;

        private float amplitude;
        private float timeLastFlicker;
        private float timeNextFlicker;
        private float pctFlickerDone;
        private float curFlickerIntensity;
        private float nextFlickerIntensity;

        private static bool isInit = false;
        private bool isStrobeInit = false;
        private bool isFlickerInit = false;

        public static void Init()
        {
            if (!isInit)
            {
                ILoggableExtensions.LogV(StaticLogTag, "Init()");
                DictEffectCoordinators = new Dictionary<string, LightEffectController>();
                isInit = true;
            }
        }

        public void InitStrobe(float timeStrobeStart, float strobePeriod, float strobeAmplitude, float strobeOffset)
        {
            if (isStrobeInit)
            {
                this.LogV("Skipping Re-Initializing of Strobe LFX Coordinator: " + coordinatorName);
                return;
            }
            this.timeStrobeStart = timeStrobeStart;
            this.strobePeriod = strobePeriod;
            this.strobeAmplitude = strobeAmplitude;
            this.strobeOffset = strobeOffset;

            effect = LightEffect.Effect.Strobe;

            isStrobeInit = true;
        }

        public void InitFlicker(float intensityMin, float intensityMax, float minFlickerTime, float maxFlickerTime)
        {
            if (isFlickerInit)
            {
                this.LogV("Skipping Re-Initializing of Flicker LFX Coordinator: " + coordinatorName);
                return;
            }
            this.intensityMin = intensityMin;
            this.intensityMax = intensityMax;
            this.minFlickerTime = minFlickerTime;
            this.maxFlickerTime = maxFlickerTime;

            effect = LightEffect.Effect.Flicker;

            isFlickerInit = true;
        }

        public static LightEffectController GetCoordinator(string name)
        {
            LightEffectController lfxCoordinator;
            if (!DictEffectCoordinators.TryGetValue(name, out lfxCoordinator))
            {
                ILoggableExtensions.LogV(StaticLogTag, "Creating LightEffectController(" + name + ")");
                lfxCoordinator = new LightEffectController();
                lfxCoordinator.coordinatorName = name;
                LightEffectController.DictEffectCoordinators.Add(name, lfxCoordinator);
            }
            else
            {
                ILoggableExtensions.LogV(StaticLogTag, "Using existing LightEffectController(" + name + ")");
            }

            return lfxCoordinator;
        }

        public void Progress()
        {
            pctFlickerDone = Mathf.Min(1.0f, (Time.time - timeLastFlicker) / (timeNextFlicker - timeLastFlicker));
            if (Time.time > timeNextFlicker)
            {
                ResetFlicker();
            }
        }

        public float GetIntensity()
        {
            switch (effect)
            {
                case LightEffect.Effect.Strobe:
                    return GetStrobeIntensity();
                case LightEffect.Effect.Flicker:
                    return GetFlickerIntensity();
                case LightEffect.Effect.Idle:
                default:
                    return intensityMax;
            }
        }

        public float GetFlickerIntensity()
        {
            return Mathf.Lerp(curFlickerIntensity, nextFlickerIntensity, pctFlickerDone);
        }

        public float GetStrobeIntensity()
        {
            return strobeAmplitude * Mathf.Cos(strobePeriod * (Time.time - timeStrobeStart)) + strobeOffset;
        }

        // Flicker helpers
        private void ResetFlicker()
        {
            pctFlickerDone = 0.0f;

            amplitude = Random.Range(0.0f, 1.0f);

            timeLastFlicker = Time.time;
            timeNextFlicker = GetNextFlickerTime(timeLastFlicker, amplitude);

            curFlickerIntensity = intensityMin;
            amplitude = pushTowardsBounds(amplitude);
            nextFlickerIntensity = GetNextFlickerIntensity(amplitude);

            this.LogV(
                string.Format("RESET ({0}): {1} -> {2} && {3} -> {4}",
                coordinatorName,
                timeLastFlicker, timeNextFlicker,
                curFlickerIntensity, nextFlickerIntensity));
        }

        private float GetNextFlickerTime(float timeLastFlicker, float amplitude)
        {
            var nextFlickerTime = timeLastFlicker + minFlickerTime + amplitude * (maxFlickerTime - minFlickerTime);
            this.LogV(
                string.Format("TIME ({0}): {1} + {2}  + {3} * {4} = {5}",
                coordinatorName, timeLastFlicker, minFlickerTime, amplitude, (maxFlickerTime - minFlickerTime), nextFlickerTime));
            return nextFlickerTime;
        }

        private float GetNextFlickerIntensity(float amplitude)
        {
            var nextFlickerIntensity = intensityMin + (amplitude * (intensityMax - intensityMin));
            this.LogV(
                string.Format("INT ({0}): {1} + {2} * {3} = {4}",
                coordinatorName, intensityMin, amplitude, (intensityMax - intensityMin), nextFlickerIntensity));
            return nextFlickerIntensity;
        }

        private float cosinerp(float v0, float v1, float t)
        {
            float ft = t * Mathf.PI;
            float f = (1 - Mathf.Cos(ft)) * 0.5f;
            return v0 * (1 - f) + v1 * f;
        }

        private float pushTowardsBounds(float val)
        {
            if (val < 0.5f)
            {
                return Mathf.Pow(val, 0.3f);
            }
            return Mathf.Sqrt(val);
        }
    }
}