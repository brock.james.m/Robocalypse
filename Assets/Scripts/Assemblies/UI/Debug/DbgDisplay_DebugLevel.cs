using TMPro;

using Robo.Base;


namespace Robo.Component.Debug
{
    public class DbgDisplay_DebugLevel : BaseComponent
    {
        protected TextMeshProUGUI textBox;


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            textBox = GetComponent<TextMeshProUGUI>();
        }

        protected override void Update()
        {
            base.Update();

            textBox.text = $"DBG LVL: {Robo.Log.Logger.Level.ToString()}";
        }

        #endregion
    }
}