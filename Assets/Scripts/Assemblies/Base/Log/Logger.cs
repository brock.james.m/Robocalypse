﻿using UnityEngine;


namespace Robo.Log
{
    public class Logger : ILog, ILoggable
    {
        /* --- ILoggable --- */
        public virtual string LogTag => StaticLogTag;
        public static string StaticLogTag => "Logger";

        private static Logger instance = null;
        public static Logger Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Logger();
                }

                return instance;
            }
        }

        public enum LogLevel
        {
            Crit = 0,
            Err,
            Warn,
            Info,
            Debug,
            Verbose
        }
        public static LogLevel Level = LogLevel.Info;

        public static void DecreaseLogLevel()
        {
            if (Level != LogLevel.Err)
            {
                Level--;
            }
        }
        public static void IncreaseLogLevel()
        {
            if (Level != LogLevel.Verbose)
            {
                Level++;
            }
        }

        public void V(string tag, string msg)
        {
            if (Level >= LogLevel.Verbose)
            {
                Log(LogLevel.Verbose, tag, msg);
            }
        }

        public void D(string tag, string msg)
        {
            if (Level >= LogLevel.Debug)
            {
                Log(LogLevel.Debug, tag, msg);
            }
        }

        public void I(string tag, string msg)
        {
            if (Level >= LogLevel.Info)
            {
                Log(LogLevel.Info, tag, msg);
            }
        }

        public void W(string tag, string msg)
        {
            if (Level >= LogLevel.Warn)
            {
                Log(LogLevel.Warn, tag, msg);
            }
        }

        public void E(string tag, string msg)
        {
            if (Level >= LogLevel.Err)
            {
                Log(LogLevel.Err, tag, msg);
            }
        }

        protected void Log(LogLevel level, string tag, string msg)
        {
            switch (level)
            {
                case LogLevel.Err:
                    Debug.LogError($"[E {tag}] {msg}\n");
                    break;
                case LogLevel.Warn:
                    Debug.LogWarning($"[W {tag}] {msg}\n");
                    break;
                case LogLevel.Info:
                    Debug.Log($"[I {tag}] {msg}\n");
                    break;
                case LogLevel.Debug:
                    Debug.Log($"[D {tag}] {msg}\n");
                    break;
                case LogLevel.Verbose:
                    Debug.Log($"[V {tag}] {msg}\n");
                    break;
            }
        }
    }
}