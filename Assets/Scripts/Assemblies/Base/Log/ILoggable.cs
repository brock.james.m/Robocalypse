namespace Robo.Log
{
    public interface ILoggable
    {
        string LogTag { get; }
    }

    public static class ILoggableExtensions
    {
        private static ILog log = Logger.Instance;


        /* --- Instance --- */
        public static void LogV(this ILoggable loggable, string msg)
        {
            log.V(loggable.LogTag, msg);
        }

        public static void LogD(this ILoggable loggable, string msg)
        {
            log.D(loggable.LogTag, msg);
        }

        public static void LogI(this ILoggable loggable, string msg)
        {
            log.I(loggable.LogTag, msg);
        }

        public static void LogW(this ILoggable loggable, string msg)
        {
            log.W(loggable.LogTag, msg);
        }

        public static void LogE(this ILoggable loggable, string msg)
        {
            log.E(loggable.LogTag, msg);
        }


        /* --- Static --- */
        public static void LogV(string logTag, string msg)
        {
            log.V(logTag, msg);
        }

        public static void LogD(string logTag, string msg)
        {
            log.D(logTag, msg);
        }

        public static void LogI(string logTag, string msg)
        {
            log.I(logTag, msg);
        }

        public static void LogW(string logTag, string msg)
        {
            log.W(logTag, msg);
        }

        public static void LogE(string logTag, string msg)
        {
            log.E(logTag, msg);
        }
    }
}