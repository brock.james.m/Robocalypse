namespace Robo.Log
{
    public interface ILog
    {
        void V(string tag, string msg);
        void D(string tag, string msg);
        void I(string tag, string msg);
        void W(string tag, string msg);
        void E(string tag, string msg);
    }
}