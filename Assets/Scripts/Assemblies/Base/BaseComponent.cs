using UnityEngine;
using Robo.Log;


namespace Robo.Base
{
    public class BaseComponent : MonoBehaviour, ILoggable
    {
        /* --- ILoggable --- */
        public string LogTag => $"{this.GetType().Name}";


        #region Lifecycle

        protected virtual void Awake()
        {
            this.LogV("Awake()");
        }

        protected virtual void OnEnable()
        {
            this.LogV("OnEnable()");
        }

        protected virtual void Start()
        {
            this.LogV("Start()");
        }

        protected virtual void Update()
        {
        }

        protected virtual void FixedUpdate()
        {
        }

        protected virtual void OnDisable()
        {
            this.LogV("OnDisable()");
        }

        protected virtual void OnDestroy()
        {
            this.LogV("OnDestroy()");
        }

        #endregion
    }
}