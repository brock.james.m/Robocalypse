﻿using System;

using UnityEngine;


namespace Robo.Util
{
    public class Vector2Interpolator : Interpolator
    {
        private Vector2Interpolator(Vector2 p0)
        : base(p0)
        { }

        private Vector2Interpolator(Vector2 p0, Vector2 p1)
        : base(p0, p1)
        { }

        private Vector2Interpolator(Vector2 p0, Vector2 p1, Vector2 p2)
        : base(p0, p1, p2)
        { }

        private Vector2Interpolator(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
        : base(p0, p1, p2, p3)
        { }

        public static IInterpolator CreateConstant(
            Vector2 p0)
        {
            return new Vector2Interpolator(p0);
        }

        public static IInterpolator CreateLinear(
            Vector2 p0,
            Vector2 p1)
        {
            return new Vector2Interpolator(p0, p1);
        }

        public static IInterpolator CreateQuadratic(
            Vector2 p0,
            Vector2 p1,
            Vector2 p2)
        {
            return new Vector2Interpolator(p0, p1, p2);
        }

        public static IInterpolator CreateCubic(
            Vector2 p0,
            Vector2 p1,
            Vector2 p2,
            Vector2 p3)
        {
            return new Vector2Interpolator(p0, p1, p2, p3);
        }

        public static IInterpolator CreateCubic()
        {
            return CreateCubic(
                new Vector2(0.0f, 0.0f),
                new Vector2(1.0f, 6.0f),
                new Vector2(2.0f, 5.0f),
                new Vector2(2.5f, 4.0f)
            );
        }
    }
}