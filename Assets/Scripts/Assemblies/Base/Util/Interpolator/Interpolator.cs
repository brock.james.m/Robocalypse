﻿using System;

using UnityEngine;

using Robo.Base;


namespace Robo.Util
{
    /// <summary>
    /// Method of interpolation
    /// </summary>
    public enum InterpolatorType
    {
        Constant,
        Linear,
        Quadratic,
        Cubic
    }

    /// <summary>
    /// Base Interpolator class. Parameters are points that represents a curve.
    /// <para>
    /// Currently only supports floats and Vector2
    /// </para>
    /// </summary>
    public class Interpolator : BaseObject, IInterpolator
    {
        protected InterpolatorType type;
        protected Vector2 p0, p1, p2, p3;

        protected Interpolator(Vector2 p0)
        {
            this.type = InterpolatorType.Constant;
            this.p0 = p0;
        }

        protected Interpolator(Vector2 p0, Vector2 p1)
        {
            this.type = InterpolatorType.Linear;
            this.p0 = p0;
            this.p1 = p1;
        }

        protected Interpolator(Vector2 p0, Vector2 p1, Vector2 p2)
        {
            this.type = InterpolatorType.Quadratic;
            this.p0 = p0;
            this.p1 = p1;
            this.p2 = p2;
        }

        protected Interpolator(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
        {
            this.type = InterpolatorType.Cubic;
            this.p0 = p0;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
        }

        public Vector2 ValueAt(float t)
        {
            Vector2 retVector = Vector2.zero;
            t = Mathf.Clamp(t, -1.0f, 1.0f);

            switch (this.type)
            {
                case InterpolatorType.Constant:
                    retVector = p0;
                    break;
                case InterpolatorType.Linear:
                    retVector = Vector3.Lerp(p0, p1, t);
                    break;
                case InterpolatorType.Quadratic:
                    retVector =
                        2 * (1.0f - t) * p0
                        + 2.0f * (1.0f - t) * t * p1
                        + 2.0f * t * p2;
                    break;
                case InterpolatorType.Cubic:
                    retVector =
                        (
                            (
                                (-p0 + 3 * (p1 - p2) + p3) * t
                                    + (3 * (p0 + p2) - 6 * p1)
                            ) * t
                            + 3 * (p1 - p0)
                        ) * t
                        + p0;
                    break;
            }

            return retVector;
        }

        public virtual float FloatValueAt(float t)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return $"{this.type} {this.GetType().Name}: ";
        }
    }
}