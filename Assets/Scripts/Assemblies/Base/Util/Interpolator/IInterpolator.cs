﻿using UnityEngine;


namespace Robo.Util
{
    public interface IInterpolator
    {
        /// <summary>
        /// Returns the value on the curve at the provided time t.
        /// </summary>
        /// <param name="t">Time [0.0f, 1.0f]</param>
        Vector2 ValueAt(float t);

        /// <summary>
        /// Returns a calculated value based on the curve at the provided time t.
        /// <para>
        /// i.e. Y value for float interpolator (not all vector data is used)
        /// </para>
        /// </summary>
        /// <param name="t">Time [0.0f, 1.0f]</param>
        float FloatValueAt(float t);
    }
}