﻿using UnityEngine;


namespace Robo.Util
{
    public class FloatInterpolator : Interpolator
    {
        private FloatInterpolator(float p0)
        : base(new Vector2(0.0f, p0))
        { }

        private FloatInterpolator(float p0, float p1)
        : base(new Vector2(0.0f, p0),
                new Vector2(0.0f, p1))
        { }

        private FloatInterpolator(float p0, float p1, float p2)
        : base(new Vector2(0.0f, p0),
                new Vector2(0.0f, p1),
                new Vector2(0.0f, p2))
        { }

        private FloatInterpolator(float p0, float p1, float p2, float p3)
        : base(new Vector2(0.0f, p0),
                new Vector2(0.0f, p1),
                new Vector2(0.0f, p2),
                new Vector2(0.0f, p3))
        { }

        public static IInterpolator CreateConstant(
            float p0 = 0.0f)
        {
            return new FloatInterpolator(p0);
        }

        public static IInterpolator CreateLinear(
            float p0 = 0.0f,
            float p1 = 0.0f)
        {
            return new FloatInterpolator(p0, p1);
        }

        public static IInterpolator CreateQuadratic(
            float p0 = 0.0f,
            float p1 = 0.0f,
            float p2 = 0.0f)
        {
            return new FloatInterpolator(p0, p1, p2);
        }

        public static IInterpolator CreateCubic(
            float p0 = 0.0f,
            float p1 = 0.0f,
            float p2 = 0.0f,
            float p3 = 0.0f)
        {
            return new FloatInterpolator(p0, p1, p2, p3);
        }

        public override float FloatValueAt(float t)
        {
            return base.ValueAt(t).y;
        }

        public override string ToString()
        {
            string output = base.ToString();

            switch (this.type)
            {
                case InterpolatorType.Constant:
                    output += $"\nConstant ({p0.y}) ".Indent(4);
                    break;
                case InterpolatorType.Linear:
                    output += $"\nLinear ({p0.y} -> {p1.y}) ".Indent(4);
                    break;
                case InterpolatorType.Quadratic:
                    output += $"\nQuadratic ({p0.y} -> {p1.y} -> {p2.y}) ".Indent(4);
                    break;
                case InterpolatorType.Cubic:
                    output += $"\nCubic ({p0.y} -> {p1.y} -> {p2.y} -> {p3.y}) ".Indent(4);
                    break;
            }

            return output;
        }
    }
}