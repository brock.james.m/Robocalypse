using UnityEngine;

using Robo.Log;
using Robo.Constants;


namespace Robo.Util
{
    public class Prefab
    {
        public static string StaticLogTag = "Util.Prefab";


        // Prefab Creation
        public static GameObject Instantiate(string prefabResourcePath)
        {
            GameObject newObject = Resources.Load(prefabResourcePath, typeof(GameObject)) as GameObject;

            if (newObject == null)
            {
                ILoggableExtensions.LogE(StaticLogTag, $"Instantiate({prefabResourcePath}) failed");
            }

            return InstantiateGameObject(newObject, newObject.name, newObject.tag);
        }

        protected static GameObject InstantiateGameObject(
            GameObject prefab,
            string name = ObjName.Unknown,
            string tag = ObjTag.Unknown)
        {
            GameObject newObject = GameObject.Instantiate(prefab) as GameObject;
            newObject.name = name;
            newObject.tag = tag;

            return newObject;
        }
    }
}