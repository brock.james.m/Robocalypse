namespace Robo.Util
{
    public static class StringExtensions
    {
        /// <summary>
        ///  2 space indent for logging
        /// </summary>
        public static int NumSpacesForIndent = 2;
        public static string Indent(this string s, int numIndents)
        {
            return s.Replace("\n", "\n" + new string(' ', numIndents));
        }
    }
}