using UnityEngine;


namespace Robo.Constants
{
    ///<summary>
    /// List of GameObject Tags, used for object identification
    ///
    /// These should be kept in sync with the Unity Editor, especially when prefabs change
    ///</summary>
    public class ObjTag
    {
        public const string Unknown = "UNKNOWN OBJECT TAG";
        public const string Player = "Player";
        public const string Enemy = "Enemy";
        public const string FriendlyProjectile = "FriendlyProjectile";
        public const string HostileProjectile = "HostileProjectile";
        public const string PassableTerrain = "PassableTerrain";
        public const string Collectible = "Collectible";
    }

    ///<summary>
    /// List of GameObject Names, used for object identification
    ///
    /// These should be kept in sync with the Unity Editor, especially when prefabs change
    ///</summary>
    public class ObjName
    {
        public const string Unknown = "UNKNOWN OBJECT NAME";
        public const string Player = "Player";
        public const string Cursor = "Cursor";
        public const string MainCamera = "MainCamera";
        public const string ProjectileSpawnPosition = "ProjectileSpawnPosition";
    }

    public class PrefabPath
    {
        public const string ViewMenu = "Prefabs/GUI/Menus/ViewMenu";
        public const string DebugOverlay = "Prefabs/GUI/DebugOverlay";
        public const string LightningBolt = "Prefabs/Projectiles/LightningBolt";
        public const string Player = "Prefabs/Player/Player";
    }

    public class SpritePath
    {
        public const string CursorLook = "Sprites/Cursor/cursor-look-sheet";
        public const string CursorAim = "Sprites/Cursor/cursor-aim-sheet";
    }

    public class Vector
    {
        public readonly Vector3 Unset = Vector3.positiveInfinity;
    }
}