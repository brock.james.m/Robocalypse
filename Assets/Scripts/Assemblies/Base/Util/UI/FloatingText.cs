﻿using UnityEngine;

using Robo.Base;
using Robo.Util.Vector;


namespace Robo.Util.UI
{
    public enum TextDepth
    {
        Highest,
        High,
        Default,
        Low,
        Lowest
    }

    public class FloatingText : BaseComponent
    {
        /// <summary>
        /// Resource defined in Unity Editor, describes text properties, color, etc
        /// <para>
        /// 'Resources/GUI/GameSkin'
        /// </para>
        /// </summary>
        private static readonly GUISkin Skin = Resources.Load<GUISkin>("GUI/GameSkin");
        public GUIStyle Style { get; set; }
        private GUIContent content;
        public string Text
        {
            get => content.text;
            set { content.text = value; }
        }

        private IFloatingTextHandler handler;


        public static FloatingText Show(
            string text,
            Vector3 spawnPosition,
            Color color,
            float alphaMin = 0.0f,
            float alphaMax = 1.0f,
            float timeToLive = 2.0f,
            int fontSize = 32,
            TextDepth depth = TextDepth.Default,
            string guiSkinStyle = "DefaultText")
        {
            // TODO: Pool these objects
            var obj = new GameObject("Floating Text");
            var floatingText = obj.AddComponent<FloatingText>();

            // TODO: Cache this
            floatingText.handler =
                FloatingTextHandler.Create(
                    spawnPosition,
                    timeToLive,
                    color,
                    alphaMin,
                    alphaMax);

            floatingText.Style = Skin.GetStyle(guiSkinStyle);
            floatingText.Style.fontSize = fontSize;
            floatingText.content = new GUIContent(text);

            return floatingText;
        }


        #region Lifecycle

        public void OnGUI()
        {
            var contentSize = Style.CalcSize(content); // size of text in PX

            // Checks if text position is valid/still updating, or should delete
            if (!handler.IsAlive)
            {
                Destroy(gameObject);
                return;
            }

            // Show that shit, homie
            Style.normal.textColor = handler.GetColor();
            var position = handler.GetPosition(content, contentSize);
            GUI.Label(new Rect(position.x, position.y, contentSize.x, contentSize.y), content, Style);
        }

        #endregion
    }
}