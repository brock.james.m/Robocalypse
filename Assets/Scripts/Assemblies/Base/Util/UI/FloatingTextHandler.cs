﻿using UnityEngine;
using Robo.Util.Vector;


namespace Robo.Util.UI
{
    public interface IFloatingTextHandler
    {
        bool IsAlive { get; }
        Vector2 GetPosition(GUIContent content, Vector2 size);
        Color GetColor();
    }

    public class FloatingTextHandler : IFloatingTextHandler
    {
        private Timer timer;
        public bool IsAlive => timer.IsActive;

        private Vector3 spawnPosition;
        private IInterpolator interpolator;

        private float xOffset;
        private float yOffset;

        public Color color;
        public float alphaMax;
        public float alphaMin;

        private FloatingTextHandler(
            Vector3 spawnPosition,
            float timeToLive,
            Color color,
            float alphaMin,
            float alphaMax)
        {
            this.spawnPosition = spawnPosition;

            this.interpolator = Vector2Interpolator.CreateCubic();

            this.color = color;
            this.alphaMin = alphaMin;
            this.alphaMax = alphaMax;

            this.timer = new Timer(timeToLive, Time.time);
        }


        public static FloatingTextHandler Create(
            Vector3 spawnPosition,
            float timeToLive,
            Color color,
            float alphaMin = 0.0f,
            float alphaMax = 1.0f)
        {
            return new FloatingTextHandler(
                spawnPosition,
                timeToLive,
                color,
                alphaMin,
                alphaMax);
        }

        public Vector2 GetPosition(GUIContent content, Vector2 size)
        {
            Vector2 offset = interpolator.ValueAt(timer.ElapsedPercent);

            var screenPos = Camera.main.WorldToScreenPoint(spawnPosition);

            return new Vector2(
                screenPos.x - (size.x / 2) - offset.x, // center the x
                Screen.height - screenPos.y - offset.y
            );
        }

        public Color GetColor()
        {
            color.a = Mathf.Lerp(alphaMax, alphaMin, timer.ElapsedPercent);

            return color;
        }
    }
}