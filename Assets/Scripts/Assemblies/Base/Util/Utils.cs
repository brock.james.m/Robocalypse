﻿using System;
using UnityEngine;
using Robo.Log;


namespace Robo.Util
{
    public class Utils
    {
        public static string StaticLogTag => "Utils";


        // Debug
        public static string MaskToLayerNames(LayerMask mask)
        {
            string res = "";
            for (int i = 0; i < 32; i++)
            {
                int layerVal = 1 << i;
                if ((mask.value & layerVal) == 1)
                {
                    res += LayerMask.LayerToName(i) + ", ";
                }
            }
            return res + "\n";
        }


        // Anchors and Alignment
        public static void StretchRectTransform(RectTransform transform)
        {
            transform.offsetMin = new Vector2(0, transform.offsetMin.y);
            transform.offsetMax = new Vector2(transform.offsetMax.x, 0);
            transform.offsetMax = new Vector2(0, transform.offsetMax.y);
            transform.offsetMin = new Vector2(transform.offsetMin.x, 0);

            // Stretch content to 100% of parent
            transform.anchorMin = new Vector2(0, 0);
            transform.anchorMax = new Vector2(1, 1);

            // Pivot at center
            transform.pivot = new Vector2(0.5f, 0.5f);
        }



        public static GameObject AddChildObject(GameObject parent, GameObject child)
        {
            child.transform.SetParent(parent.transform, worldPositionStays: false);
            child.transform.position = parent.transform.position;
            return child;
        }

        public static GameObject GetChildByName(GameObject parent, string childName)
        {
            return parent.transform.Find(childName)?.gameObject;
        }
    }
}