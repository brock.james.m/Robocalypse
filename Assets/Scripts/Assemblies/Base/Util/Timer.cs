using UnityEngine;

using Robo.Base;


namespace Robo.Util
{
    public class Timer : BaseObject
    {
        private float duration;
        private float startTime;
        private float endTime;

        /// <summary>
        /// Create an 'active' timer
        /// </summary>
        /// <param name="duration"></param>
        /// <param name="startTime"></param>
        public Timer(float duration, float startTime)
        {
            this.duration = duration;
            this.startTime = startTime;
            this.endTime = startTime + duration;
        }

        /// <summary>
        /// Create an 'inactive' timer, to be Reset on first use
        /// </summary>
        /// <param name="duration"></param>
        public Timer(float duration)
        {
            this.duration = duration;
            this.startTime = 0.0f;
            this.endTime = 0.0f;
        }

        public void Reset(float startTime)
        {
            this.startTime = startTime;
            this.endTime = startTime + duration;
        }

        public bool IsActive => (Time.time < endTime);
        public float TimeLeft => IsActive ? (endTime - Time.time) : 0.0f;
        public float ElapsedTime => (duration - TimeLeft);
        /// <summary>
        /// Value [0.0f, 1.0f] indicating percent completion. Used in interpolators.
        /// </summary>
        /// <returns></returns>
        public float ElapsedPercent => Mathf.Clamp(ElapsedTime / duration, 0.0f, 1.0f);
    }
}
