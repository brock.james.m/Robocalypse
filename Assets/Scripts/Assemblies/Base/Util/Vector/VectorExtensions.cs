using UnityEngine;


namespace Robo.Util.Vector
{
    public static class VectorExtensions
    {
        // Default precision for Vector values is 3 decimal places
        private const float DefaultPrecision = 0.001f;
        private const float DefaultNormalizedDistance = 1.0f; // same as builtin Vector.normalized


        #region Vector2

        /// <summary>
        /// Edit X or Y
        /// </summary>
        /// <param name="me"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static Vector2 With(this Vector2 me, float? x = null, float? y = null)
        {
            return new Vector2(
                x ?? me.x,
                y ?? me.y
            );
        }

        /// <summary>
        /// Return new Vector3 from this Vector2 with an added Z value
        /// </summary>
        /// <param name="me"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static Vector3 With(this Vector2 me, float z)
        {
            return new Vector3(
                me.x,
                me.y,
                z
            );
        }

        /// <summary>
        /// Equal to (other - this)
        /// </summary>
        /// <param name="me"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static Vector2 To(this Vector2 me, Vector2 other)
        {
            return (other - me);
        }

        /// <summary>
        /// True if the X and Y of this vector are within 'precision' of the X and Y of 'other'
        /// </summary>
        /// <param name="me"></param>
        /// <param name="other"></param>
        /// <param name="precision"></param>
        /// <returns></returns>
        public static bool EqualsWithPrecision(this Vector2 me, Vector2 other, float precision)
        {
            Vector3 diff = (other - me);

            if (Mathf.Abs(diff.x) > precision) return false;
            return Mathf.Abs(diff.y) <= precision;
        }

        /// <summary>
        /// True if this vector is the same as 'other' to DefaultPrecision (3 decimals)
        /// </summary>
        /// <param name="me"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool RoughlyEquals(this Vector2 me, Vector2 other)
        {
            return me.EqualsWithPrecision(other, DefaultPrecision);
        }


        /// <summary>
        /// Normalize a vector's magnitude to a specified distance
        /// </summary>
        /// <param name="me"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static Vector2 Normalize(this Vector2 me, float distance)
        {
            if (me.x == 0)
            {
                return new Vector2(
                    0.0f,
                    (me.y > 0) ? distance
                        : (me.y == 0.0f) ? 0.0f
                            : -distance);
            }

            float newXsign = Mathf.Sign(me.x);
            float slope = me.y / me.x;

            float scale = distance
                / Mathf.Sqrt((Mathf.Pow(me.x, 2) + Mathf.Pow(me.y, 2)));
            float scaledDsquared = Mathf.Pow(scale, 2) * (Mathf.Pow(me.x, 2) + Mathf.Pow(me.y, 2));
            float newX = newXsign * Mathf.Sqrt(scaledDsquared
                                                / (Mathf.Pow(slope, 2) + 1)
                                                );
            float newY = newX * slope;
            return new Vector2(newX, newY);
        }


        /// <summary>
        /// Limits a vector's magnitude to a specified distance
        /// </summary>
        /// <param name="me"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static Vector2 LimitDistance(this Vector2 me, float distance)
        {
            return (me.magnitude > distance)
                ? me.Normalize(distance)
                : me;
        }


        /// <summary>
        /// Return this vector rotated 'degrees' counter-clockwise from Vector2.up
        /// </summary>
        /// <param name="me"></param>
        /// <param name="degrees"></param>
        /// <returns></returns>
        public static Vector2 Rotate(this Vector2 me, float degrees)
        {
            float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

            float tx = me.x;
            float ty = me.y;
            me.x = (cos * tx) - (sin * ty);
            me.y = (sin * tx) + (cos * ty);

            return me;
        }

        /// <summary>
        /// Return number of degrees clockwise from Vector2.up this vector lies
        /// </summary>
        /// <param name="me"></param>
        /// <param name="degrees"></param>
        /// <returns></returns>
        public static float DegreesClockwiseFromUp(this Vector2 me)
        {
            float rotationRads = Mathf.Atan2(me.y, me.x);
            return rotationRads * (180.0f / Mathf.PI);
        }

        #endregion

        #region Vector3

        /// <summary>
        /// Edit X, Y or Z
        /// </summary>
        /// <param name="me"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Vector3 With(this Vector3 me, float? x = null, float? y = null, float? z = null)
        {
            return new Vector3(
                x ?? me.x,
                y ?? me.y,
                z ?? me.z
            );
        }

        /// <summary>
        /// Equal to (other - this)
        /// </summary>
        /// <param name="me"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static Vector3 To(this Vector3 me, Vector3 other)
        {
            return (other - me);
        }

        /// <summary>
        /// True if the XYZ of this vector are within 'precision' of the XYZ of 'other'
        /// </summary>
        /// <param name="me"></param>
        /// <param name="other"></param>
        /// <param name="precision"></param>
        /// <returns></returns>
        public static bool EqualsWithPrecision(this Vector3 me, Vector3 other, float precision)
        {
            Vector3 diff = (other - me);

            if (Mathf.Abs(diff.x) > precision) return false;
            if (Mathf.Abs(diff.y) > precision) return false;
            return Mathf.Abs(diff.z) <= precision;
        }


        /// <summary>
        /// True if this vector is the same as 'other' to DefaultPrecision (3 decimals)
        /// </summary>
        /// <param name="me"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool RoughlyEquals(this Vector3 me, Vector3 other)
        {
            return me.EqualsWithPrecision(other, DefaultPrecision);
        }


        /// <summary>
        /// Normalize a vector's magnitude to a specified distance
        /// <para>
        /// Vector3.z forced to -1
        /// </para>
        /// </summary>
        /// <param name="me"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static Vector3 Normalize(this Vector3 me, float distance)
        {
            if (me.x == 0)
            {
                return new Vector3(
                    0.0f,
                    (me.y > 0) ? distance
                        : (me.y == 0.0f) ? 0.0f
                            : -distance,
                    -1.0f);
            }

            float newXsign = Mathf.Sign(me.x);
            float slope = me.y / me.x;

            float scale = distance
                / Mathf.Sqrt((Mathf.Pow(me.x, 2) + Mathf.Pow(me.y, 2)));
            float scaledDsquared = Mathf.Pow(scale, 2) * (Mathf.Pow(me.x, 2) + Mathf.Pow(me.y, 2));
            float newX = newXsign * Mathf.Sqrt(scaledDsquared
                                                / (Mathf.Pow(slope, 2) + 1)
                                                );
            float newY = newX * slope;
            return new Vector3(newX, newY, -1.0f);
        }


        /// <summary>
        /// Limits a vector's magnitude to a specified distance
        /// </summary>
        /// <param name="me"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static Vector3 LimitDistance(this Vector3 me, float distance)
        {
            return (me.magnitude > distance)
                ? me.Normalize(distance)
                : me;
        }

        #endregion

    }
}