using UnityEngine;


namespace Robo.Util.Vector
{
    public interface IFacingDirection
    {
        Vector2 FacingDirection { get; set; }
    }
}