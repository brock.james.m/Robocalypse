using Robo.Log;


namespace Robo.Base
{
    public class BaseObject : ILoggable
    {
        /* --- ILoggable --- */
        public string LogTag => $"{this.GetType().Name}";
    }
}