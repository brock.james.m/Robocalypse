using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;

using Robo.Base;
using Robo.Input;
using Robo.Log;
using Robo.Util;



namespace Robo.Menu
{
    public class BaseMenu : BaseComponent
    {
        protected List<string> pageNames;
        protected List<Dictionary<string, UnityAction>> buttonsByPage;

        protected int curPageIndex = 0;
        protected int curButtonIndex = 0;
        protected float buttonSwitchCooldownTime = 0.12f;
        protected float lastButtonSwitchTime = 0.0f;

        private List<GameObject> pageObjs;
        private List<GameObject> buttonObjs;


        /* --- Public API --- */
        public static void CreateMenuFromPrefab(GameObject parentObj, string menuPrefabPath)
        {
            GameObject menu = Prefab.Instantiate(menuPrefabPath);
            Utils.AddChildObject(parentObj, menu);
        }


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            pageObjs = new List<GameObject>();
            buttonObjs = new List<GameObject>();

            LoadMenuPages();
            SelectPage(0);
        }

        protected override void OnEnable()
        {
            Time.timeScale = 0;

            SetupActions();

            InputManager.UseActionMap(InputManager.InputActions.Menu);
        }

        protected override void OnDisable()
        {
            Time.timeScale = 1;

            DisableActions();

            InputManager.UsePrevActionMap();
        }

        #endregion


        #region Actions

        protected void SetupActions()
        {
            InputManager.InputActions.Menu.Move.performed += OnMove;
            InputManager.InputActions.Menu.Select.performed += OnSelect;
            InputManager.InputActions.Menu.Back.performed += OnBack;

            InputManager.InputActions.Menu.ViewMenu.performed += OnViewMenu;
            InputManager.InputActions.Menu.GameMenu.performed += OnGameMenu;
        }

        protected void DisableActions()
        {
            InputManager.InputActions.Menu.Move.performed -= OnMove;
            InputManager.InputActions.Menu.Select.performed -= OnSelect;
            InputManager.InputActions.Menu.Back.performed -= OnBack;

            InputManager.InputActions.Menu.ViewMenu.performed -= OnViewMenu;
            InputManager.InputActions.Menu.GameMenu.performed -= OnGameMenu;
        }

        protected void OnMove(InputAction.CallbackContext ctx)
        {
            Vector2 movement = ctx.ReadValue<Vector2>();

            if (movement.y > 0.0f)
            {
                OnUp();
            }
            else if (movement.y < 0.0f)
            {
                OnDown();
            }
        }

        protected virtual void OnUp()
        {
            SelectPreviousButton();
        }

        protected virtual void OnDown()
        {
            SelectNextButton();
        }

        protected void OnSelect(InputAction.CallbackContext ctx)
        {
            buttonObjs[curButtonIndex]
                .GetComponent<Button>()
                .onClick.Invoke();
        }

        protected virtual void OnBack(InputAction.CallbackContext ctx)
        {
            throw new NotImplementedException();
        }

        protected virtual void OnViewMenu(InputAction.CallbackContext ctx)
        {
            Destroy(this.gameObject);
        }

        protected void OnGameMenu(InputAction.CallbackContext ctx)
        {
            throw new NotImplementedException();
        }

        #endregion


        #region Navigation

        protected void SelectPage(string pageName)
        {
            SelectPage(pageNames.IndexOf(pageName));
        }

        protected void SelectPage(int pageIndex)
        {
            this.LogD("SelectPage()");

            curPageIndex = pageIndex;

            for (int i = 0; i < pageObjs.Count; i++)
            {
                pageObjs[i].SetActive(curPageIndex == i);
            }

            LoadButtonsForPage(curPageIndex);

            curButtonIndex = 0;
            HighlightButton(curButtonIndex);
        }

        protected void HighlightButton(int index)
        {
            for (int i = 0; i < buttonObjs.Count; i++)
            {
                buttonObjs[i]
                    .GetComponentInChildren<TMPro.TextMeshProUGUI>()
                    .color = (i == index)
                        ? new Color32(0, 255, 0, 192)
                        : new Color32(255, 255, 255, 192);
            }
        }

        protected void SelectPreviousButton()
        {
            if (!IsAllowedButtonSwitch())
            {
                return;
            }

            // C# mod is funky with negative numbers
            curButtonIndex = (curButtonIndex + buttonObjs.Count - 1) % buttonObjs.Count;
            HighlightButton(curButtonIndex);
        }

        protected void SelectNextButton()
        {
            if (!IsAllowedButtonSwitch())
            {
                return;
            }

            curButtonIndex = (curButtonIndex + buttonObjs.Count + 1) % buttonObjs.Count;
            HighlightButton(curButtonIndex);
        }

        protected bool IsAllowedButtonSwitch()
        {
            // Use unscaled time when menu is open (game paused, Time.timescale = 0)
            if (Time.unscaledTime > lastButtonSwitchTime + buttonSwitchCooldownTime)
            {
                lastButtonSwitchTime = Time.unscaledTime;
                return true;
            }

            return false;
        }

        #endregion


        #region Init

        private void LoadMenuPages()
        {
            this.LogD("LoadMenuPages()");

            pageObjs.Clear();

            foreach (string pageName in this.pageNames)
            {
                GameObject page = Utils.GetChildByName(this.gameObject, pageName);
                pageObjs.Add(page);
            }
        }

        public delegate void UnityAction();
        private void LoadButtonsForPage(int pageIndex)
        {
            this.LogD($"LoadButtonsForPage({pageIndex})");

            buttonObjs.Clear();

            foreach (KeyValuePair<string, UnityAction> button in buttonsByPage[pageIndex])
            {
                string buttonName = $"{pageNames[pageIndex]}/Buttons/{button.Key}";
                this.LogD($"Loading: {buttonName}");
                GameObject buttonObj = Utils.GetChildByName(this.gameObject, $"{buttonName}");

                buttonObj.GetComponent<Button>().onClick.AddListener(delegate { button.Value(); });
                buttonObjs.Add(buttonObj);
            }
        }

        #endregion
    }
}