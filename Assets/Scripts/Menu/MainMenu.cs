using System.Collections.Generic;


namespace Robo.Menu
{
    public class MainMenu : BaseMenu
    {
        /*--- Lifecycle --- */
        protected override void Awake()
        {
            pageNames = new List<string>()
        {
            "Main",
        };
            buttonsByPage = new List<Dictionary<string, UnityAction>>()
        {
            // Main Selection
            new Dictionary<string, UnityAction>()
            {
            },
        };

            base.Awake();
        }
    }
}