using System.Collections.Generic;

using Robo.Log;


namespace Robo.Menu
{
    public class ViewMenu : BaseMenu
    {
        public const string PrefabPath = Constants.PrefabPath.ViewMenu;


        #region Lifecycle

        protected override void Awake()
        {
            pageNames = new List<string>()
            {
                "Main",
                "Controls"
            };

            buttonsByPage = new List<Dictionary<string, UnityAction>>()
            {
                // Main Selection
                new Dictionary<string, UnityAction>()
                {
                    {
                        "Resume", () => { Destroy(this.gameObject); }
                    },
                    {
                        "Controls", () => { SelectPage("Controls"); }
                    },
                    {
                        "Quit", () => {/*TODO*/ }
                    },
                },
                // Controls
                new Dictionary<string, UnityAction>()
                {
                    {
                        "Back", () => { SelectPage("Main"); }
                    },
                },
            };

            base.Awake();
        }

        #endregion
    }
}