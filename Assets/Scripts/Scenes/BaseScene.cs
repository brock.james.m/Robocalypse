﻿using UnityEngine;
using UnityEngine.SceneManagement;

using Robo.Base;
using Robo.Constants;
using Robo.Util;


namespace Robo.Component.SceneManager
{
    /// <summary>
    /// Base class for any Unity scene
    /// </summary>
    /// <remarks>
    /// Provides basic functionality and access for a scene
    /// </remarks>
    public abstract class BaseScene : BaseComponent
    {
        protected Scene unityScene;
        protected SceneManager sceneManager;

        public static bool ShowDebug = true; // TODO: Add toggle somewhere
        GameObject debugDisplay;


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            Cursor.visible = false;

            sceneManager = SceneManager.Instance;
            unityScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();

            debugDisplay = Prefab.Instantiate(PrefabPath.DebugOverlay);
        }

        protected override void Start()
        {
            base.Start();

            sceneManager.OnSceneStart();
        }

        protected override void Update()
        {
            base.Update();

            debugDisplay.SetActive(ShowDebug);
        }

        #endregion
    }
}
