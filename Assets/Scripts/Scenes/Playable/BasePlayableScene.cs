﻿using UnityEngine;
using Robo.Log;


namespace Robo.Component.SceneManager.Playable
{
    /// <summary>
    /// Base class for any Unity scene with a playable character
    /// </summary>
    /// <remarks>
    /// Should override lifecycle methods to load any player - related objects
    /// Right now, I can think of:
    /// - Player
    /// </remarks>
    public abstract class BasePlayableScene : BaseScene
    {
        public GameObject player;
        public GameObject playerSpawn;


        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();
            InitPlayableScene();
        }

        protected override void Start()
        {
            base.Start();
            StartPlayableScene();
        }

        protected override void Update()
        {
            base.Update();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            DestroyPlayableScene();
        }

        #endregion


        /* --- Initialize components, set properties --- */
        protected void InitPlayableScene()
        {
        }

        /* --- Position, link, and trigger components --- */
        protected void StartPlayableScene()
        {
            StartPlayer();
        }

        protected void StartPlayer()
        {
            this.LogD($"Moving player to spawn position: {playerSpawn.transform.position}");
            player.transform.position = playerSpawn.transform.position;
        }

        /* --- Tear down references, clean up --- */
        protected void DestroyPlayableScene()
        {
        }
    }
}