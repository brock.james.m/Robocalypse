using UnityEngine;
using Robo.Input;


namespace Robo.Component.SceneManager.Playable
{
    public class Level1 : BasePlayableScene
    {
        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();

            Physics2D.gravity = Vector2.zero;

            Robo.Input.InputManager.CurrentDevice = CurrentDevice.KeyboardAndMouse;
        }

        #endregion
    }
}