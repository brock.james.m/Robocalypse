using Robo.Menu;


namespace Robo.Component.SceneManager.Menu
{
    /// <summary>
    /// Main Menu
    /// </summary>
    public class MainMenuScene : BaseMenuScene
    {
        protected override void Awake()
        {
            // TODO: Add prefab
            menu = gameObject.AddComponent<MainMenu>();
        }
    }
}