using Robo.Menu;


namespace Robo.Component.SceneManager.Menu
{
    /// <summary>
    /// Provides basic functionality for loading and running a menu scene
    /// Uses menu control scheme
    /// </summary>
    public class BaseMenuScene : BaseScene
    {
        protected BaseMenu menu;
    }
}