namespace Robo.Component.SceneManager.Loader
{

    /* 
    * Initial scene that should
    *   - Load game related stuff
    *   - Init global stuff (logger, singleton scene controller, etc)
    *   - Kick off to the main visible scene: MainMenu
    */
    public class GameLoader : BaseScene
    {
        #region Lifecycle

        protected override void Awake()
        {
            InitGame();

            base.Awake(); // First time scene awake should happen after game init
        }

        protected override void Update()
        {
            LoadMainMenu();
        }

        #endregion


        /* --- Helpers --- */
        private void InitGame()
        {
        }

        private void LoadMainMenu()
        {
            sceneManager.LoadScene(SceneName.MainMenu);
        }
    }
}