﻿using System.Collections;
using UnityEngine;

using Robo.Component;
using Robo.Log;


namespace Robo.Component.SceneManager
{
    public class SceneManager : SingletonComponent<SceneManager>, ILoggable
    {
        public string LogTag => $"{this.GetType().Name}";

        protected bool isSceneLoading = false;
        public bool IsSceneLoading { get { return isSceneLoading; } }

        /// <summary>
        /// Should only be called by BaseScene.Start()
        /// </summary>
        public void OnSceneStart()
        {
            isSceneLoading = false;
        }

        public void LoadScene(string sceneName)
        {
            this.LogI($"Loading '{sceneName}'");

            StartCoroutine(LoadAsync(sceneName));

            isSceneLoading = true;
        }

        public void LoadScene(SceneName sceneNameEnum)
        {
            LoadScene(sceneNameEnum.ToString());
        }

        public void ReloadScene()
        {
            LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        }

        private IEnumerator LoadAsync(string sceneName)
        {
            AsyncOperation op = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName);

            while (!op.isDone)
            {
                float progress = Mathf.Clamp01(op.progress / 0.9f);
                // TODO: use in loading screen
                // also where did this 0.9f come from?

                yield return null;
            }
        }

        // Should time NOT be in the scenemanager?
        public void StopTime(bool stop)
        {
            if (stop)
            {
                StopTime();
            }
            else
            {
                ResumeTime();
            }
        }

        public void StopTime()
        {
            UnityEngine.Time.timeScale = 0;
        }

        public void ResumeTime()
        {
            UnityEngine.Time.timeScale = 1;
        }
    }
}