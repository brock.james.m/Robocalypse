namespace Robo.Component.SceneManager
{
    public enum SceneName
    {
        GameLoader,
        MainMenu,
        Level1
    }
}