[Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)

# **Robocalypse**

# TODO

## In progress

- So what the fuck is this game supposed to be, exactly?
  - Real time combat? Could be for the roguelite part (in the simulation)
    - Battery = health in the simulation?
  - Turn based combat? Could be for the real world exploration (slower pace, can have puzzles)
    - Battery = action points?

## High Priority

- Basic Enemy AI (states)
- Add Equipment (battery)
- Add Socketables (chips?)
  - For character upgrades
  - For world upgrades (charger stations, quests/story interactions, trading?)
- Class out interactables (workstation)
  - Add Equipment management (workstation)
  - Add charging stations (workstation)
- Add Abilities/Perks/Chips/Boards
- Menu and GUI systems

## Standard Priority

- Input listing, remapping and UI
- Improved lighting system
- Floating text

## Nice to haves

- [Unity Analytics](https://docs.unity3d.com/ScriptReference/Analytics.AnalyticsSessionInfo.html)
- Script to find commentless/summary-less classes, functions?
- Input Remapping

# Architecture

[LucidChart UML Diagrams](https://lucid.app/documents#/documents?folder_id=271134602&browser=icon)

Key:

- Blue: Component (Implements MonoBehaviour)
- Yellow: Interface
- Green: Object (Does not implement MonoBehaviour)
- Purple: Enum
- Orange: Assembly

![Assemblies](./Docs/ReadmeImages/Assemblies.jpeg?raw=true "Assemblies")
![BaseComponent](./Docs/ReadmeImages/BaseComponent.jpeg?raw=true "BaseComponent")
![TriggerEventComponent](./Docs/ReadmeImages/TriggerEventComponent.jpeg?raw=true "TriggerEventComponent")

### Directory Structure

- **Robocalypse/Assets/Scripts/**
  - **Assemblies/**
    - All sub directories are compiles into distinct assemblies
    - 'Interface' assemblies MUST ONLY reference Robo.Base and Unity assemblies
      - This allows using, for example, the ICursor interface without needing all Input assemblies
  - **Everything else**
    - Uses the assemblies.
    - Should be Top-Level components (character, scene, etc)

### Scenes

- **Scripts/Scenes**
  - Scenes are managed by the SceneManager singleton instance
  - BaseScene talks to the SceneManager to reload, change scenes
  - Each Scene will need
    - World gameobject, the root holder object
      - Holds Scene instance
      - Holds InputManager
      - Everything in the level should be a child of World
        - Tilemaps
        - Locations (spawnpoints, triggers, etc)
        - Lighting
        - Objects (player, enemy, collectable, etc)
        - Camera

# Unity Lifecycle

- Lifecycle
  ![MonoBehaviour Lifecycle](https://docs.unity3d.com/uploads/Main/monobehaviour_flowchart.svg)

# Tips/tricks

### TODO Finder

```
grep -P '//\s*TODO' $(find . -name "*.cs")
```
