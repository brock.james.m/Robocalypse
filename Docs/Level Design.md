[Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)

# Scale
- We are using a reference scale of 16x16 pixels per unit in unity
- Maybe 1 unit = 1 meter?
    - Player is 1.5 units tall (16x24 px)
    - Roofs
        - Low roofs (houses, sheds, etc) should use walls of height 2 units 

# Level Design
- These will probably be informed by mechanics and their progression, from unlocking to mastering
- 'Levels' is a loose abstraction of a period of time during the game
    - Level 1 is about learning basic character mechanics
    - Level 2 is about learning basic world/story mechanics
    - Level... is about improving skills and aqcuiring gear, exploring to do so
    - ...
    - Last level, final boss?, is a display of all mechanics

## Act 1 - The Beginning
### Intro
- Meet character and major NPC
    - Battery is dead/malfunctioning
        - keeps resetting until friend NPC gives you a better, still broken, battery
    - Wake up and get a brief summary of the world (post apocalpytic?)
    - Get a tour of the main base?

### Level 1
- Basic exploration and game mechanics
    - Timer (battery): 1 min?
    - Help NPC gather basic resources
        - Use these resources to craft something (some sort of progression)

### Level 2
- First minor quest, unlocking new area
    - Go hit a switch or something, I don't know


### Level 3
- First major quest, unlocking a new zone


# Gameplay references:
## Gameplay thoughts
[Rational Enemy Design](https://gdkeys.com/keys-to-rational-enemy-design/)
- We want to have a variety of enemies/attacks/situations
- Generalizing/Abstracting these into their core components will help us create unique and varied gameplay

- Movement
    - Melee enemies charge player: player must move away
    - Ranged enemies aim at player: player must move away
    - Ranged enemies spread shot: player must move away accordingly
    - Melee enemies with radius trigger (e.g. bomb plant): player must avoid area
    - Ranged enemies place ground runes: player must avoid area
    - Strong Melee enemies have cleave attack with safe zone next to enemy: player must seek area
    - Strong Ranged enemies attack all except a safe zone: player must seek area
- Block/Counter
    - Ranged enemies fire seeking missle: player must block/counter


## Gameplay ideas
- Camera zoom in/out (foreground changes, look behind things, reveals, etc new things)
- Day cycle (battery drain)
    - Battery is on a timer (like minit), will reset after hitting 0
        - 1-time-use rechargers in the world?
            - maybe unlockable? upgradeable? **socketable?**
    - Maybe collectable first phase 
        - Can drop off stuff in box to save it per run
        - If you run out of time, you keep some stuff (light stuff? what qualifies?) but lose the rest
    - World reset at next cycle
    - Player keeps some inventory, lose buffs, etc
- Encounters: 
    - Daily 'STS ?' events, such as:
        - interactions
            - talking to other characters
            - 'spirits' or 'wall runes' that trigger
            - using machines
        - breaking things (people's property, rocks, pots, pets!, etc)

